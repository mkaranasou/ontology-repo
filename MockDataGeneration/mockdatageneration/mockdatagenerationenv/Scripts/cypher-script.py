#!C:\xampp\htdocs\MockDataGeneration\mockdatageneration\mockdatagenerationenv\Scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'py2neo==2.0.6','console_scripts','cypher'
__requires__ = 'py2neo==2.0.6'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('py2neo==2.0.6', 'console_scripts', 'cypher')()
    )
