__author__ = 'm.karanasou'
import os
from mockdatageneration.helpers.globals import g
from mockdatageneration.models.POSTag import pos_tagger
from mockdatageneration.models.TextSimilarity import similarity_finder
from mockdatageneration.models.FakeDataGeneration import fake_data
from mockdatageneration.views.main import *
from mockdatageneration.views.ontology import *
from mockdatageneration.views.mappings_suggestion import *


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8082))
    app.run(host='0.0.0.0', port=port, debug=True, threaded=True)
