import ast
import copy
import nltk
from numpy.ma import transpose
import scipy
from scipy.spatial.distance import pdist
import sklearn
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.metrics.pairwise import linear_kernel
from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.svm import NuSVC
from sklearn.tree import DecisionTreeClassifier
from mockdatageneration.helpers.globals import g
from sklearn.ensemble import RandomForestRegressor
from nltk.metrics import precision, recall, f_measure
from sklearn.metrics import classification_report
# pairwise cosine= http://stackoverflow.com/questions/12118720/python-tf-idf-cosine-to-find-document-similarity

__author__ = 'maria'


class Classifier(object):
    """
        Wrapper class for scikit-learn classifiers.
    """
    def __init__(self, cls_type, vectorizer_type, use_pairwise_cosine=False, use_tf_transformer=True):
        self.estimator = sklearn.svm.LinearSVC()
        self.cls_type = cls_type
        self.cls = self._set_up_classifier(cls_type)
        self.vec = self._set_up_vectorizer(vectorizer_type)
        self.Xtrain = None
        self.Y = None
        self.Xtrial = None
        self.YTrial = None
        self.predictions = []
        self.XtrainCopy = None
        self.use_pairwise_cosine = use_pairwise_cosine
        self.use_tf_transformer = use_tf_transformer
        self.tfidf_transformer = TfidfTransformer(use_idf=False)
        self.sklearn_precision_score = ""

    def _set_up_classifier(self, cls_type):
        if cls_type == g.CLASSIFIER_TYPE.NBayes:
            return MultinomialNB()
        elif cls_type == g.CLASSIFIER_TYPE.SVM:
            return OneVsRestClassifier(estimator=sklearn.svm.LinearSVC())
        elif cls_type == g.CLASSIFIER_TYPE.DecisionTree:
            return DecisionTreeClassifier()
        elif cls_type == g.CLASSIFIER_TYPE.SVMStandalone:
            return sklearn.svm.LinearSVC(C=1)
        elif cls_type == g.CLASSIFIER_TYPE.SVC:
            return sklearn.svm.SVC(C=1, gamma=0.001)
        elif cls_type == g.CLASSIFIER_TYPE.SVR:
            return sklearn.svm.SVR(kernel='linear', max_iter=150)  # >> 150
        elif cls_type == g.CLASSIFIER_TYPE.NuSVR:
            return sklearn.svm.NuSVR(kernel='linear', max_iter=10)
        elif cls_type == g.CLASSIFIER_TYPE.RandomForestRegressor:
            return RandomForestRegressor(n_estimators=2, max_depth=50)
        elif cls_type == g.CLASSIFIER_TYPE.SGD:
            return SGDClassifier(loss='perceptron', n_iter=25)
        else:
            raise NotImplementedError

    def _set_up_vectorizer(self, vectorizer_type):
        if vectorizer_type == g.VECTORIZER.Dict:
            return DictVectorizer(sparse=False)
        elif vectorizer_type == g.VECTORIZER.Count:
            return CountVectorizer(analyzer='word',
                                   input='content',
                                   lowercase=True,
                                   min_df=1,
                                   binary=False,
                                   stop_words='english')
        elif vectorizer_type == g.VECTORIZER.Idf:
            return TfidfVectorizer(use_idf=True, stop_words='english')
        elif vectorizer_type == g.VECTORIZER.Tf:
            return TfidfVectorizer(use_idf=False, stop_words='english')
        else:
            raise NotImplementedError

    def set_x_train(self, train_list_of_dicts):
        self.Xtrain = self.vec.fit_transform(train_list_of_dicts)
        g.logger.debug(train_list_of_dicts[0])
        g.logger.debug(self.Xtrain[0:1])

        if self.use_pairwise_cosine:
            self.XtrainCopy = copy.copy(self.Xtrain)
            self.Xtrain = sklearn.metrics.pairwise.pairwise_distances(self.XtrainCopy[0:1], self.XtrainCopy, metric='cosine', n_jobs=1)
            # self.Xtrain = linear_kernel(self.Xtrain[0:1], self.Xtrain)
        if self.use_tf_transformer:
            self.Xtrain = self.tfidf_transformer.transform(self.Xtrain).toarray()
        # g.logger.debug("feature_names: %s" % self.vec.feature_names_)
        # g.logger.debug("vocabulary_: %s" % self.vec.vocabulary_)
        g.logger.debug("Xtrain: %s" % self.Xtrain)

    def set_y_train(self, handcoded_score_list):
        self.Y = handcoded_score_list
        g.logger.debug("Y:: %s" % self.Y)

    def train(self):
        print self.Xtrain.shape[0], len(self.Y)
        if self.use_pairwise_cosine:
            self.cls.fit(transpose(self.Xtrain), self.Y)      # transpose is used when cosine similarities are calculated
        else:
            self.cls.fit(self.Xtrain, self.Y)

    def set_x_trial(self, trial_list_of_dicts):
        self.Xtrial = self.vec.transform(trial_list_of_dicts)
        if self.use_pairwise_cosine:
            self.Xtrial = sklearn.metrics.pairwise.pairwise_distances(self.XtrainCopy[0:1], self.Xtrial, metric='cosine', n_jobs=1)
            # self.Xtrial = linear_kernel(self.Xtrial[0:1], self.Xtrial)
            # x = pdist(transpose(self.Xtrial.toarray()), 'cosine')
        if self.use_tf_transformer:
            self.Xtrial = self.tfidf_transformer.transform(self.Xtrial).toarray()

    def set_y_trial(self, trial_scores):
        self.YTrial = trial_scores

    def predict(self):
        if self.use_pairwise_cosine:
            self.predictions = self.cls.predict(transpose(self.Xtrial)).tolist()
        else:
            self.predictions = self.cls.predict(self.Xtrial).tolist()
        # print classification_report(self.YTrial, self.predictions)
        return self.predictions

    def get_metrics(self, vec):
        self.get_most_informant_features(self.cls, vec)
        self.sklearn_precision_score = sklearn.metrics.precision_score(self.YTrial,
                                                                       self.predictions,
                                                                       labels=self.YTrial,
                                                                       average='weighted')
        print self.sklearn_precision_score
        print "precision:", nltk.metrics.precision(set(self.YTrial), set(self.predictions))
        print "recall:", nltk.metrics.recall(set(self.YTrial), set(self.predictions))
        print "f_measure:", nltk.metrics.f_measure(set(self.YTrial), set(self.predictions))

    def get_most_informant_features(self, classifier, vec, n=10):
        print "IMPORTANT FEATURES:\n"

        feature_names = vec.get_feature_names()
        if self.cls_type != g.CLASSIFIER_TYPE.DecisionTree and self.cls_type != g.CLASSIFIER_TYPE.RandomForestRegressor\
                and self.cls_type != g.CLASSIFIER_TYPE.SVR:
            coefs_with_fns = sorted(zip(classifier.coef_[0], feature_names))
            top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1])

            for (coef_1, fn_1), (coef_2, fn_2) in top:
                important_feature = "\t%.4f\t%-15s\n\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2)
                print important_feature
                g.logger.debug("important_feature %s" % important_feature)
