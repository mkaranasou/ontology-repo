__author__ = 'm.karanasou'

class Item(object):
    def __init__(self):
        pass


class InternalItem(Item):
    def __init__(self, line):
        super(InternalItem, self).__init__()
        self.line = line.strip("\n\r")
        self.tap_separated_line = self.line.split("\t")
        self.category = self.tap_separated_line[1]
        self.brand_category = self.tap_separated_line[0]
        self.split_line = self.tap_separated_line[3].split(" ")
        self.skip = 0
        self.item_quantity = self._get_item_quantity()
        self.quantity_type = self._get_quantity_type()
        self.how_many = self._get_how_many()
        self.brand = self._get_brand()
        self.others = self._get_others()
        self.country = self._get_country()

    def _get_item_quantity(self):
        if len(self.split_line[0]) < 2:
            self.skip = 1
            return self.split_line[0] + "." + self.split_line[1]
        return self.split_line[0]

    def _get_quantity_type(self):
        if self.skip > 0:
            return self.split_line[2][:3]
        return self.split_line[1][:3]

    def _get_how_many(self):
        if self.skip > 0:
            index = 3
        else:
            index = 2
        if len(self.split_line[index]) == 3:
            return self.split_line[index]
        else:
            return self.split_line[index-1][4:]

    def _get_brand(self):
        if self.skip > 0:
            index = 4
        else:
            index = 3
        return self.split_line[index]

    def _get_others(self):
        if self.skip > 0:
            index = 5
        else:
            index = 4
        return self.split_line[index:-2]

    def _get_country(self):
        return self.split_line[-2:]



class IncomingItem(Item):
    def __init__(self, line):
        super(IncomingItem, self).__init__()
        self.line = line.strip("\n\r")
        self.separated_line = ""
        self.category = ""
        self.brand_category = ""
        self.split_line = ""
        self.split_text = None
        self.brand = None
        self.numbers = None
        self.metrics = None
        self.others_metric_wise = None
        self.greeklish = None
        self.english_translation = None
        self.item_quantity = self._get_item_quantity()
        self.quantity_type = self._get_quantity_type()
        self.how_many = self._get_how_many()

    def _get_item_quantity(self):
        pass

    def _get_quantity_type(self):
        pass

    def _get_how_many(self):
        pass

    def _get_brand(self):
        pass

