__author__ = 'm.karanasou'


class ExceptionHandler(object):
    def __init__(self, name, type_, **kwargs):
        self.name = name
        self.kwargs = kwargs
        if type_ >= 100 or type_ <= 510:
            self.type_ = type_
        else:
            raise Exception("Wrong exception type {0}".format(type(type_)))
        self.message = self._get_message_per_type()

    def _get_message_per_type(self):
        if self.type_ == ExceptionType.missing_parameter:
            return "Missing '{0}' parameter".format(str(self.name))
        if self.type_ == ExceptionType.wrong_type_for_parameter:
            return "Wrong type for parameter '{0}': '{1}'".format(self.name,
                                                                  self.kwargs["data_type"]
                                                                  if "data_type" in self.kwargs
                                                                  else "unknown")
        if self.type_ == ExceptionType.other:
            return "Server Exception occurred"


class ExceptionType(object):
    """
        Main exception types and their corresponding code
    """
    # todo: double- check codes
    missing_parameter = 400
    wrong_type_for_parameter = 400
    other = 500
