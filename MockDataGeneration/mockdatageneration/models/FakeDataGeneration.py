import random
from faker import Factory
from mockdatageneration import pos_tagger
from mockdatageneration.helpers.pyenchant_spell_checker import EnchantSpellChecker


__author__ = 'm.karanasou'


class FakeDataGenerator(object):
    def __init__(self):
        self.class_name = ""
        self.class_category = ""
        self.attributes = []
        self.times_repeat = 1
        self.spell_checker = EnchantSpellChecker()
        self.first_name = ["name", "firstname", "fname", "first"]
        self.last_name = ["lastname", "surnamename", "sname", "last", "lname"]
        self.full_name = ["fullname"]
        self.age = ["age"]
        self.address = ["address", "street", "streetname"]
        self.fake = Factory.create()
        self.init_spellchecker()
        self.ambiguous = ["name", "street"]
        self.already_seen = []
        self.pos_tagger = pos_tagger

    def get_data(self, faker_category, data_type):
        if faker_category is None:
            if data_type == "int":
                return random.randint(10, 80)
            if data_type == "string":
                return self.fake.word()
            if data_type == "DateTime":
                return self.fake.date()
            if data_type == "bool":
                return self.fake.boolean(chance_of_getting_true=50)
            else:
                return self.fake.word()
        if faker_category == 'age':
            return random.randint(10, 80)
        try:
            func = eval('self.fake.' + faker_category)
        except:
            # for occasions where category asked is not available from faker
            func = self.fake.word
        return func()

    def init_spellchecker(self):
        self.spell_checker.dict_exists('en')

    def generate(self, class_name, class_category, attributes, times_repeat):
        self.class_name = class_name
        self.class_category = class_category
        self.attributes = attributes
        self.times_repeat = times_repeat

        # 1. split attributes in case more than one
        split_attr = attributes.split(',')
        result = {}

        # 2. iterate through attributes
        for attribute in split_attr:
            temp_attr = attribute.replace(",", "")
            faker_data = []

            for each in range(0, self.times_repeat):
                self.preprocess(temp_attr)
                faker_data.append(self.get_faker_data(class_category, class_name,  temp_attr))
            result[temp_attr] = faker_data
            self.already_seen.append(temp_attr)

    def preprocess(self, attribute):
        # 1. check it attribute exists as is in the declared categories
        category_list = self.exists_as_is(attribute)
        if len(category_list) > 0:
            return category_list

        # 2. if not try split on punctuation


        # 3. if no punctuation try split on camel or pascal case

        # 4. if none of the above worked try a classifier maybe???

        pass

    def exists_as_is(self, attribute):
        for category_list in self.get_category_list():
            if attribute.lower() in category_list:
                return category_list

        return []

    # def check_spelling(self, word):
    #     pass
    #
    # def split_camel_or_pascal_case(self, word):
    #     split_attr = [a for a in re.split(r'([A-Z][a-z]*\d*)', str(word)) if a]
    #     return split_attr
    #
    # def get_class_category(self, class_name):
    #     pass
    #
    # def get_attr_category(self, attr, class_name):
    #     # 1. check it attribute exists as is in the declared categories
    #     category_list = self.exists_as_is(attr)
    #     if len(category_list) > 0:
    #         return category_list
    #
    #     else:
    #         # 2. Check if we have Current_customer
    #         split_category = attr.split('_')
    #
    #         if len(split_category) > 1:
    #
    #             # Try get noun
    #             noun = self.pos_tagger.pos_tag(split_category)
    #             print noun
    #             if noun is not None and len(noun) > 2:
    #                 # check again
    #                 category_list = self.exists_as_is(noun)
    #
    #                 if len(category_list) > 0:
    #                     return category_list
    #                 else:
    #                     pass
    #                     # try something else, e.g. a classifier
    #         else:
    #             # 3. Check if we have CurrentCustomer or currentCustomer
    #             split_category = self.split_camel_or_pascal_case(attr)
    #             if len(split_category) > 1:
    #                 noun = self.pos_tagger.pos_tag(split_category)
    #                 print noun
    #                 if noun is not None and len(noun) > 2:
    #                     category_list = self.exists_as_is(noun)
    #
    #                     if len(category_list) > 0:
    #                         return category_list
    #                     else:
    #                         pass
    #                         # try something else, e.g. a classifier
    #
    #     return ["unknown"]
    #
    # def remove_punctuation(self, word):
    #     word = re.replace('_', ' ', word)
    #     return word
    #
    # def get_faker_data(self, class_name, category, attr):
    #     name = ["name", "firstname", "fname", "first"]
    #     last_name = ["lastname", "surnamename", "sname", "last", "lname"]
    #     full_name = ["fullname"]
    #     age = ["age"]
    #     address = ["address", "street", "streetname"]
    #
    #     if category == "Human":
    #         attr_lower = attr.lower()
    #         if attr_lower in name:
    #             return self.fake.first_name()
    #         elif "fullname" in attr_lower:
    #             return self.fake.name()
    #         elif attr_lower in last_name:
    #             return self.fake.last_name()
    #         elif attr_lower in age:
    #             return random.randint(10, 80)
    #         elif attr_lower in address:
    #             if "address" in attr_lower:
    #                 return self.fake.address()
    #             elif "street" in attr_lower:
    #                 return self.fake.street()
    #             return self.fake.address
    #     elif category == "Address":
    #         return self.fake.address()
    #     elif category == "Animal":
    #         pass
    #     elif category == "Computer":
    #         return self.fake.ipv4()
    #     elif category == "Company":
    #         return self.fake.company()
    #     elif category == "Card":
    #         return self.fake.credit_card_full(card_type=None, validate=False, max_check=10)
    #     elif category=="Date":
    #         return self.fake.date_time_this_century()
    #
    #     return self.fake.word()  # lorem ipsum..
    #
    # def get_category_list(self):
    #     return [self.first_name, self.full_name, self.last_name, self.age, self.address]


fake_data = FakeDataGenerator()
