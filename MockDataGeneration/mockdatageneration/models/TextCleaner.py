# -*- coding: utf-8 -*-
from nltk.tokenize import word_tokenize
from mockdatageneration.models.ItemModel import IncomingItem
import re
import goslate

__author__ = 'm.karanasou'

BRANDS = []
greek = "αβγδεζηθικλμνξοπρστυφχψω"
conversion_chars = {
        u'α': 'a',
        u'β': 'b',
        u'γ': 'c',
        u'δ': 'd',
        u'ε': 'e',
        u'ζ': 'z',
        u'η': 'i',
        u'θ': 'th',
        u'ι': 'i',
        u'κ': 'k',
        u'λ': 'l',
        u'μ': 'm',
        u'ν': 'n',
        u'ξ': 'ks',
        u'ο': 'o',
        u'π': 'p',
        u'ρ': 'r',
        u'σ': 's',
        u'τ': 't',
        u'υ': 'y',
        u'φ': 'f',
        u'χ': 'x',
        u'ψ': 'ps',
        u'ω': 'w',
    }

class TextCleaner(object):
    def __init__(self, text):
        self.text = text
        self.incoming_item = IncomingItem(self.text)
        self.split_text = None
        self.brand = None
        self.numbers = None
        self.metrics = None
        self.others_metric_wise = None
        self.gs = goslate.Goslate()

    def process(self):
        self._split()
        self._normalize()

    def _split(self):
        self.split_text = word_tokenize(self.text)

    def _normalize(self):
        for word in self.split_text:
            word = word.lower()                                     # 1. convert to lower
            is_number = len(re.findall(r'\d+', word)) > 0           # 2. find if it contains numbers
            is_in_greek = len([x for x in word if x in greek]) > 0  # if any greek it needs conversion (or translation)

            if is_number:
                self._handle_numbers(word, is_in_greek)
            if not is_in_greek and word in BRANDS:
                self.brand = word
                continue
            if is_in_greek:
                self._to_greeklish(word)
                self._to_english(word)
            
    def _handle_numbers(self, word, is_in_greek):
        word = word.replace(",", ".").replace("*", "x")
        if is_in_greek:
            word = word.replace("χ", "x")

        self.numbers = re.findall(r"([0-9]+[.,]?[0-9]*)", word)  # ([0-9]+[.,]?[0-9]*) (\d\D{0,9}\d+)
        self.metrics = re.findall(r"([a-z]{1,3})", word)
        self.others_metric_wise = re.findall(r"([a-z]{1,3})", word)

    def _to_greeklish(self, word):
        new_word = ""
        for char in word:
            if char in conversion_chars:
                new_word += conversion_chars[char]
            else:
                new_word += char
        return new_word

    def _to_english(self, word):
        """
        Translate
        :param word:
        :return:
        """
        return self.gs.translate(word, 'en')
