import copy
from mockdatageneration import NameHelper
from mockdatageneration.models.ComparisonProvider import ComparisonProvider
from mockdatageneration.models.Match import Match, MatchType
from mockdatageneration.models.ModelTree import TransformationModel
import xml.etree.ElementTree as et

__author__ = 'm.karanasou'


class SuggestionHandler(object):
    def __init__(self, xml_model):
        self.xml_model = xml_model
        self.name_helper = NameHelper()
        self.model = None
        self.common_classes = []
        self.common_attributes_per_class = {}
        self.suggestions = []
        self.comparison_provider = ComparisonProvider()
        self.class_matches = []
        self.attribute_matches = []

    def load_model(self):
        self.model = TransformationModel(et.fromstring(self.xml_model))
        self.model.load()

    def suggest(self):
        self.common_classes.append([self.model.left_tree, self.model.right_tree])

        # AS IS
        self._find_common_classes(self.model.left_tree.subclasses, self.model.right_tree.subclasses)
        # SYNONYMS
        for cls in self.model.left_tree.subclasses:
            if not cls.is_matched:
                for each in self.model.right_tree.subclasses:
                    comp_result = self.comparison_provider.synonyms_comparison(cls, each)
                    if comp_result > 0:
                        cls.is_matched = True
                        each.is_matched = True
                        match = Match()
                        match.add([cls, each], MatchType.synonyms, comp_result)
                        self.common_classes.append([cls, each])
                        self.class_matches.append(match)
                        break
        # SIMILARITY
        for cls in self.model.left_tree.subclasses:
            self._match_class_to_class_by_conceptnet_assertion(cls, self.model.right_tree)

        # AS IS FOR REMAINING CLASSES to ATTRIBUTES - NO INNER ATTRIBUTE MATCHING
        for cls in self.model.left_tree.subclasses:
            self._match_class_to_attribute(cls, self.model.right_tree)

        self._find_common_attributes_for_classes()

        # root attributes
        for attr in self.model.left_tree.attributes:
            if not attr.is_matched:
                for each in self.model.right_tree.attributes:
                    comp_result = self.comparison_provider.as_is_comparison(attr, each)
                    if comp_result >= 50:
                        attr.is_matched = True
                        each.is_matched = True
                        attr.match.add([attr, each], MatchType.synonyms, comp_result)
                        self.suggestions.append([attr, each])
                        break
                    comp_result = self.comparison_provider.synonyms_comparison(attr, each)
                    if comp_result >= 70:
                        attr.is_matched = True
                        each.is_matched = True
                        attr.match.add([attr, each], MatchType.synonyms, comp_result)
                        self.suggestions.append([attr, each])
                        break
                # SIMILARITY
                if not attr.is_matched:
                    for each in self.model.right_tree.attributes:
                        comp_result = self.comparison_provider.semantic_similarity(attr, each)
                        if comp_result >= 0.4:
                            attr.is_matched = True
                            each.is_matched = True
                            self.suggestions.append([attr, each])
                            attr.match.add([attr, each], MatchType.semantic_similarity, comp_result)
                            break

        return self.suggestions

    def _match_class_to_attribute(self, left, right):
        comp_result = 0.0
        if not left.is_matched and left.name != "DbTimestamp":
            for each in right.attributes:
                if not each.is_matched:
                    comp_result = self.comparison_provider.as_is_comparison(left, each)
                    if comp_result >= 0.4:
                        left.is_matched = True
                        each.is_matched = True
                        left.match.add([left, each], MatchType.as_is, comp_result)
                        match = Match()
                        match.add([left, each], MatchType.semantic_similarity, comp_result)
                        self.class_matches.append(match)
                        self.suggestions.append([left, each])
                        break
        if not left.is_matched or comp_result < 100:
            if left.name != "DbTimestamp":
                for subc in right.subclasses:
                    self._match_class_to_attribute(left, subc)

        for left_sub in left.subclasses:
            self._match_class_to_attribute(left_sub, right)

    def _match_class_to_class_by_conceptnet_assertion(self, left, right):
        if not left.is_matched and left.name != "DbTimestamp":
            for each in right.subclasses:
                if not each.is_matched:
                    comp_result = self.comparison_provider.semantic_similarity(left, each)
                    if comp_result >= 0.4:
                        left.is_matched = True
                        self.common_classes.append([left, each])
                        left.match.add([left, each], MatchType.semantic_similarity, comp_result)
                        match = Match()
                        match.add([left, each], MatchType.semantic_similarity, comp_result)
                        self.class_matches.append(match)
                        break
        if not left.is_matched and left.name != "DbTimestamp":
            for subc in right.subclasses:
                self._match_class_to_attribute(left, subc)

        for left_sub in left.subclasses:
            self._match_class_to_attribute(left_sub, right)

    def _find_common_classes(self, subclasses_left, subclasses_right):
        for subclass_left in subclasses_left:
            self._find_class_match(subclass_left, subclasses_right)
            for each in subclass_left.subclasses:
                    self._find_class_match(each, subclasses_right)

    def _find_class_match(self, cls, subclass_list):
        comp_result = 0.0

        for subclass_right in subclass_list:
            comp_result = self.comparison_provider.as_is_comparison(cls, subclass_right)
            print "comp_result for {0}- {1} as is :{2}".format(cls.name, subclass_right.name, comp_result)
            if comp_result > 0.5:
                cls.is_matched = True
                cls.match.add([cls, subclass_right], MatchType.as_is, comp_result)
                self.common_classes.append([cls, subclass_right])
                match = Match()
                match.add([cls, subclass_right], MatchType.as_is, comp_result)
                self.class_matches.append(match)

        # if class couldn't be found or match was not 100 percent then look into subclasses
        if not cls.is_matched or comp_result < 100:
            for subclass_right in subclass_list:
                self._find_class_match(cls, subclass_right.subclasses)

    def _find_common_attributes_for_classes(self):
        for cls_pair in self.common_classes:
            both_arrays = 0
            # tmp_left = copy.copy(cls_pair[0])
            # tmp_right = copy.copy(cls_pair[1])
            if len(cls_pair[0].attributes) == 0 and \
                    ("Collection" in cls_pair[0].data_type or "Array" in cls_pair[0].data_type):
                if len(cls_pair[0].subclasses) > 0:
                    cls_pair[0] = cls_pair[0].subclasses[0]
                    both_arrays += 1
            if len(cls_pair[1].attributes) == 0 and \
                    ("Collection" in cls_pair[1].data_type or "Array" in cls_pair[1].data_type):
                if len(cls_pair[1].subclasses) > 0:
                    cls_pair[1] = cls_pair[1].subclasses[0]
                    both_arrays += 1
            # if both_arrays > 1:
            #     self.suggestions.append([tmp_left, tmp_right])
            # else:
            #     if "Collection" in tmp_left.parent.data_type or "Array" in tmp_left.parent.data_type and\
            #         "Collection" in tmp_right.parent.data_type or "Array" in tmp_right.parent.data_type:
            #         self.suggestions.append([tmp_left, tmp_right])

            for attr in cls_pair[0].attributes:
                comp_result = 0.0
                for right_attr in cls_pair[1].attributes:
                    comp_result = self.comparison_provider.as_is_comparison(attr, right_attr)
                    print "comp_result AS IS for {0}- {1} as is :{2}".format(attr.preprocessed_pseydoname, right_attr.preprocessed_pseydoname, comp_result)
                    if comp_result >= 50 and not right_attr.is_matched:
                        attr.is_matched = True
                        right_attr.is_matched = True
                        if [attr, right_attr] not in self.suggestions:
                            self.suggestions.append([attr, right_attr])
                            attr.match.add([attr, right_attr], MatchType.as_is, comp_result)
                            break

                # if not attr.is_matched:
                #
                #     if comp_result < 100:
                #         for right_attr in cls_pair[1].attributes:
                #             comp_result = self.comparison_provider.synonyms_comparison(attr, right_attr)
                #
                #             if comp_result >= 0.9 and not right_attr.is_matched:
                #                 attr.is_matched = True
                #                 right_attr.is_matched = True
                #
                #                 if [attr, right_attr] not in self.suggestions:
                #                     self.suggestions.append([attr, right_attr])
                #                     attr.match.add([attr, right_attr], MatchType.synonyms, comp_result)
                #                     break

                if not attr.is_matched:
                    if comp_result < 100:
                        for right_attr in cls_pair[1].attributes:
                            comp_result = self.comparison_provider.semantic_similarity(attr, right_attr)
                            # print "COMP_RESULT ", comp_result
                            if comp_result >= 0.4 and not right_attr.is_matched:
                                attr.is_matched = True
                                right_attr.is_matched = True
                                # print attr.name, " could match ", right_attr.name
                                # print attr.preprocessed_name, " could match ", right_attr.preprocessed_name
                                if [attr, right_attr] not in self.suggestions:
                                    self.suggestions.append([attr, right_attr])
                                    attr.match.add([attr, right_attr], MatchType.semantic_similarity, comp_result)
                                    break

    def _collection_condition(self, left, right):
        pass
        # both_arrays = 0
        # tmp_left = copy.copy(left)
        # tmp_right = copy.copy(right)
        #
        # # if left is a collection
        # if len(left.attributes) == 0 and \
        #         ("Collection" in left.data_type or "Array" in left.data_type):
        #     left = left.subclasses[0]
        #     both_arrays += 1
        # elif "Collection" in left.parent.data_type or "Array" in left.parent.data_type:
        #     both_arrays += 1
        # if len(right.attributes) == 0 and \
        #         ("Collection" in right.data_type or "Array" in right.data_type):
        #     right = right.subclasses[1]
        #     both_arrays += 1\
        # elif "Collection" in left.parent.data_type or "Array" in left.parent.data_type:
        #     both_arrays += 1
        #
        # if both_arrays > 1:
        #     self.suggestions.append([tmp_left, tmp_right])
        # else:
        #     if "Collection" in tmp_left.parent.data_type or "Array" in tmp_left.parent.data_type and\
        #         "Collection" in tmp_right.parent.data_type or "Array" in tmp_right.parent.data_type:
        #         self.suggestions.append([tmp_left, tmp_right])

    def _find_common_attributes_by_synonyms(self):
        # for attr in self.model.left_tree.attributes:
        #    if not attr.is_matched:
        #        for each in self.model.right_tree.attributes:
        #            if self.comparison_provider.synonyms_comparison(attr, each):
        #                attr.is_matched = True
        #                each.is_matched = True
        #                self.suggestions.append([attr, each])
        #                break
        #    # SIMILARITY
        #    if not attr.is_matched:
        #        for each in self.model.right_tree.attributes:
        #             if self.comparison_provider.concept_net_assertion_comparison(attr, each) > 0:
        #                attr.is_matched = True
        #                each.is_matched = True
        #                self.suggestions.append([attr, each])
        #                break
        pass

    def _format_results(self):
        pass