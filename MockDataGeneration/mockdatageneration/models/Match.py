__author__ = 'm.karanasou'


class Match(object):
    def __init__(self):
        self.pairs = []
        self.methods = []
        self.confidence = []
        self.compatible_data_types = self._init_data_types()

    def _init_data_types(self):
        return {
            "string": ["string", "int", "decimal", "float", "bool"],
            "float": ["float", "decimal", "int", "string"],
            "decimal": ["decimal", "float"],
            "bool": ["bool", "int", "string"],
            "DateTime": ["DateTime", "string", "long"]
        }

    def add(self, pairs, method, confidence):
        self.pairs.append(pairs)
        self.methods.append(method)
        self.confidence.append(confidence)

    def get_best_match(self):
        found = None
        if len(self.pairs) == 1:
            return [self.pairs[0], self.methods[0], self.confidence[0]]

        for i in range(0, len(self.pairs)):
            if self.methods[i] == "" and self.confidence[i] > 50:
                if self.pairs[i][0].data_type in self.compatible_data_types:

                    found = [self.pairs[i], self.methods[i], self.confidence[i]]
        if found is None:
            found = [self.pairs[0], self.methods[0], self.confidence[0]]

        return found


class MatchType(object):
    as_is = "As Is"
    synonyms = "Synonyms"
    semantic_similarity = "Semantic Similarity"