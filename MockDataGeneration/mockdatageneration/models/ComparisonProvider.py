import json
import re
import requests
from nltk import WordNetLemmatizer
from mockdatageneration.helpers.pyenchant_spell_checker import EnchantSpellChecker
from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic
brown_ic = wordnet_ic.ic('ic-brown.dat')

__author__ = 'm.karanasou'


class ComparisonProvider(object):
    def __init__(self):
        self.lemmatizer = WordNetLemmatizer()
        self.spell_checker = EnchantSpellChecker()
        self.spell_checker.dict_exists('en')
        self.compatible_data_types = self._init_data_types()
        self.comparison_types = ['as_is_comparison', 'synonyms_comparison', 'concept_net_assertion_comparison']

    def _init_data_types(self):
        return {
            "string": ["string", "int", "decimal", "float", "bool"],
            "float": ["float", "decimal", "int", "string"],
            "decimal": ["decimal", "float"],
            "bool": ["bool", "int", "string"]
        }

    def as_is_comparison(self, left, right):
        match = 0.0

        # if right.is_matched:
        #     return False

        percent = 0.0

        #  if left.pseydoname.lower() in right.pseydoname.lower() or\
        #    right.pseydoname.lower() in left.pseydoname.lower() or\
        #    left.name.lower() in right.name.lower() or\
        #    right.name.lower() in left.name.lower():
        for each in left.preprocessed_name:
            if each in right.preprocessed_name:
                match += 1.0

        percent = self._match_percentage(match, left.preprocessed_name)
        if percent >= 50:
            return percent
        match = 0.0
        for each in left.preprocessed_pseydoname:
            if each in right.preprocessed_pseydoname:
                match += 1.0
        percent = self._match_percentage(match, left.preprocessed_pseydoname)
        if percent >= 50:
            return percent

        len_left_name = len(left.name)
        left_name_lower = left.name.lower()
        len_left_pseydoname = len(left.pseydoname)
        left_pseydoname_lower = left.pseydoname.lower()

        len_right_name = len(right.name)
        right_name_lower = right.name.lower()
        len_right_pseydoname = len(right.pseydoname)
        right_pseydoname_lower = right.pseydoname.lower()

        if left_pseydoname_lower in right_pseydoname_lower or\
           right_pseydoname_lower in left_pseydoname_lower or\
           left_name_lower in right_name_lower or\
           right_name_lower in left_name_lower:

            if len_left_pseydoname > len_right_pseydoname:
                if left.pseydoname.lower() in right.pseydoname.lower():
                    matched_chars = len(left_pseydoname_lower.replace(right_pseydoname_lower, ""))
                    return (float(matched_chars) / float(len_left_pseydoname)) * 100
                    # return (float(matched_chars) / (float(len_left_pseydoname + len_left_name)/2.0)) * 100
            if len_left_pseydoname <= len_right_pseydoname:
                if right.pseydoname.lower() in left.pseydoname.lower():
                        matched_chars = len(left_pseydoname_lower.replace(right_pseydoname_lower, ""))
                        return (float(matched_chars) / float(len_right_pseydoname)) * 100
                        # return (float(matched_chars) / (float(len_right_pseydoname + len_right_name)/2.0)) * 100

            if len_left_name > len_right_name:
                if left.name.lower() in right.name.lower():
                    matched_chars = len(left_name_lower.replace(right_name_lower, ""))
                    return (float(matched_chars) / float(len_left_pseydoname)) * 100
                    # return (float(matched_chars) / (float(len_left_pseydoname + len_left_name)/2.0)) * 100

            if len_left_name <= len_right_name:
                if right.name.lower() in left.name.lower():
                    matched_chars = len(right_name_lower.replace(left_name_lower, ""))
                    return (float(matched_chars) / float(len_left_pseydoname)) * 100
                    # return (float(matched_chars) / (float(len_left_pseydoname + len_left_name)/2.0)) * 100
            return True

        return 0.0

    @staticmethod
    def _match_percentage(match, name_list):
        if len(name_list) > 0:
            return float(match/len(name_list)) * 100.0
        return 0.0

    @staticmethod
    def by_numbers_comparison(left, right):
        found_left = re.findall('\d', left.name)
        found_left_pseydo = re.findall('\d', left.name)
        found_right = re.findall('\d', right.name)
        found_right_pseydo = re.findall('\d', right.name)

        if len(found_left) > 0 and found_left == found_right:
            return True
        if len(found_left_pseydo) > 0 and found_left_pseydo == found_right_pseydo:
            return True
        return False

    def lemma_comparison(self, left, right):
        if len(left.preprocessed_name) > 1:
            self.lemmatizer.lemmatize(left.name)
        else:
            self.lemmatizer.lemmatize(left.name)

    def semantic_similarity(self, left, right, use_conceptnet=True):
        final_score = 0.0
        try:
            if use_conceptnet:
                final_score += self.concept_net_assertion_comparison(left, right)
            final_score += self.wordnet_similarity(left, right)
        except:
            return 0.0

        if final_score > 0 and use_conceptnet:
            return (final_score/2.0)
        elif final_score > 0 and not use_conceptnet:
            return final_score
        return 0.0

    def wordnet_similarity(self, left, right):
        score = 0.0
        times = 0.0
        try:
            if left.use_preprocessed_pseydoname:
                for each in left.preprocessed_pseydoname:
                    a = wn.synset('{0}.n.01'.format(each.lower()))
                    if right.use_preprocessed_pseydoname:
                        for right_pseydo in right.preprocessed_pseydoname:
                            b = wn.synset('{0}.n.01'.format(right_pseydo.lower()))
                            # score += wn.res_similarity(a, b, brown_ic)
                            score += wn.wup_similarity(a, b)
                            times += 1.0
                    else:
                        b = wn.synset('{0}.n.01'.format(right.pseydoname.lower()))
                        # score += wn.res_similarity(a, b, brown_ic)
                        score += wn.wup_similarity(a, b)
                        times += 1.0
            else:
                if right.use_preprocessed_pseydoname:
                    a = wn.synset('{0}.n.01'.format(left.pseydoname.lower()))
                    for right_pseydo in right.preprocessed_pseydoname:
                        b = wn.synset('{0}.n.01'.format(right_pseydo.lower()))
                        # score += wn.res_similarity(a, b, brown_ic)
                        score += wn.wup_similarity(a, b)
                        times += 1.0
                else:
                    a = wn.synset('{0}.n.01'.format(left.pseydoname.lower()))
                    b = wn.synset('{0}.n.01'.format(right.pseydoname.lower()))
                    # score += wn.res_similarity(a, b, brown_ic)
                    score += wn.wup_similarity(a, b)
                    times += 1.0

            return score/times if times > 0 else 0
        except:
            return 0

    @staticmethod
    def concept_net_isa_comparison(left, right):
        """
        http://conceptnet5.media.mit.edu/data/5.2/search?rel=/r/IsA&start=/c/en/car&limit=10
        example response:
          "edges": [
            {
              "context": "/ctx/all",
              "dataset": "/d/globalmind",
              "end": "/c/en/vehicle",
              "endLemmas": "vehicle",
              "features": [
                "/c/en/car /r/IsA -",
                "/c/en/car - /c/en/vehicle",
                "- /r/IsA /c/en/vehicle"
              ],
              "id": "/e/75c980a3cd4b484eb14c19a324de8443bbe2cd78",
              "license": "/l/CC/By-SA",
              "nodes": [
                "/r/IsA",
                "/c/en/vehicle",
                "/c/en/car"
              ],
              "rel": "/r/IsA",
              "relLemmas": "",
              "score": 43.09439,
              "source_uri": "/or/[/and/[/s/activity/globalmind/assert/,/s/contributor/globalmind/fi/heiki/]/,/and/[/s/activity/globalmind/assert/,/s/contributor/globalmind/us/openmind/]/,/and/[/s/activity/omcs/commons2_template/,/s/contributor/omcs/nbatfai/]/,/and/[/s/activity/omcs/commons2_template/,/s/contributor/omcs/trimalchio/]/,/and/[/s/activity/omcs/globalmind/,/s/contributor/omcs/heiki/]/,/and/[/s/activity/omcs/globalmind/,/s/contributor/omcs/openmind/]/,/and/[/s/activity/omcs/vote/,/s/contributor/omcs/Craleb/]/,/and/[/s/activity/omcs/vote/,/s/contributor/omcs/charlesbobo/]/]",
              "sources": [
                "/s/activity/globalmind/assert",
                "/s/activity/omcs/commons2_template",
                "/s/activity/omcs/globalmind",
                "/s/activity/omcs/vote",
                "/s/contributor/globalmind/fi/heiki",
                "/s/contributor/globalmind/us/openmind",
                "/s/contributor/omcs/Craleb",
                "/s/contributor/omcs/charlesbobo",
                "/s/contributor/omcs/heiki",
                "/s/contributor/omcs/nbatfai",
                "/s/contributor/omcs/openmind",
                "/s/contributor/omcs/trimalchio"
              ],
              "start": "/c/en/car",
              "startLemmas": "car",
              "surfaceText": "A [[car]] is a [[vehicle]].",
              "text": [
                "car",
                "",
                "vehicle"
              ],
              "timestamp": "2014-04-16T23:14:39.106Z",
              "uri": "/a/[/r/IsA/,/c/en/car/,/c/en/vehicle/]",
              "weight": 4.2479277
            }]
        :param left:
        :param right:
        :return:
        """
        url = 'http://conceptnet5.media.mit.edu/data/5.2/search?rel=/r/IsA&start=/c/en/{0}&limit=10'

        url_left = url.format(left.name.lower())
        url_right = url.format(right.name.lower())
        response_left = requests.get(url_left)
        response_right = requests.get(url_right)

    @staticmethod
    def concept_net_assertion_comparison(left, right):
        """
        response:
            {
              "similar": [
                [
                  "/c/en/person_offspring",
                  0.56173075898889224
                ]
              ],
              "terms": [
                [
                  "/c/en/human",
                  1.0
                ]
              ]
            }
        :param left:
        :param right:
        :return:
        """
        response = []
        score = 0.0
        times = 0.0
        url = 'http://conceptnet5.media.mit.edu/data/5.2/assoc/c/en/{0}?filter=/c/en/{1}&limit=1'

        if left.use_preprocessed_pseydoname:
            for each in left.preprocessed_pseydoname:
                if right.use_preprocessed_pseydoname:
                    for right_pseydo in right.preprocessed_pseydoname:
                        response.append(requests.get(url.format(each, right_pseydo)))
                        times += 1.0
                else:
                    response.append(requests.get(url.format(each, right.pseydoname.lower())))
                    times += 1.0
        else:
            if right.use_preprocessed_pseydoname:
                for right_pseydo in right.preprocessed_pseydoname:
                    response.append(requests.get(url.format(left.pseydoname.lower(), right_pseydo)))
                    times += 1.0
            else:
                response.append(requests.get(url.format(left.pseydoname.lower(), right.pseydoname.lower())))
                times += 1.0

        for resp in response:
            data = json.loads(resp.text)
            if data is not None and len(data["similar"]) > 0:
                if data["similar"][0][0].replace("/c/en/", "") in right.preprocessed_pseydoname:
                    if "/neg" in data["similar"][0][0][-4:]:
                        score -= data["similar"][0][1]
                    else:
                        score += data["similar"][0][1]  # return score

        return score/times if times > 0 else 0.0

    def synonyms_comparison(self, left, right):
        # todo: antonyms
        match = 0.0
        len_left = len(left.synonyms)
        len_right = len(right.synonyms)

        for each in left.synonyms:
            if each in right.synonyms:
                match += 1.0

        if len_left > 0 and len_right > 0:
            return (match/float((len_right + len_left)/2)) * 100
        return match

    def character_wise_comparison(self, left, right):
        pass
