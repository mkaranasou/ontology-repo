import json
import traceback
from faker import Factory
from mockdatageneration.models.FakeDataGeneration import FakeDataGenerator
from mockdatageneration.data.attribute_categories import AttributeCategories
from mockdatageneration.data.class_categories_per_attribute_categories import Categories
from mockdatageneration.models.TextSimilarity import similarity_finder
from mockdatageneration.models.ExceptionHandler import ExceptionHandler, ExceptionType
from mockdatageneration.models.RequestClass import RequestClass

__author__ = 'm.karanasou'


class RequestHandler(object):
    req_type_to_func = {
        "/api/1.0/tests/faker": "get_faker_data",
        "/api/1.0/categories/attributes": "get_attribute_categories",
        "/api/1.0/categories/class": "get_class_categories",
        "/api/1.0/categories/get_gategory_for_class/": "get_gategory_for_class",
        "/api/1.0/data/get_data": "generate_data",
        "/api/1.0/data/get_name": "get_proper_name",
        "/api/1.0/data/get_data_by_category": "get_data_by_category",
    }

    def __init__(self, request):
        self.request = request
        self.classes = []
        self.repeat = 0
        self.route = request.path
        self.method = request.method

    def process(self):
        if self.route in self.req_type_to_func:
            return self._process_post()
        else:
            raise NotImplementedError("Process for route {0} is not implemented".format(self.route))

    def _process_post(self):
        func = eval('self._' + self.req_type_to_func[self.route])
        return func()

    def _process_get(self):
        pass

    def _get_repeat(self):
        if "repeat" in self.request.form:
            self.repeat = int(self.request.form["repeat"])

    def _get_classes(self):
        if "classes" in self.request.form:
            self.classes = self.request["classes"]

    def _get_faker_data(self):
        fake = Factory.create()
        return fake.profile()

    def _get_attribute_categories(self):
        return AttributeCategories

    def _get_class_categories(self):
        return Categories

    def _get_gategory_for_class(self):
        if "class_name" in self.request and self.method == "GET":
            return similarity_finder.find_from_wordnet(self.request["class_name"])
        return ExceptionHandler("class_name", ExceptionType.missing_parameter)

    def _generate_data(self):
        final_name = ""
        repeat = None
        try:
            if self.request.method == "POST":
                classes = []
                results = []
                if len(self.request.form.keys()) > 0:
                    classes = json.loads(self.request.form["classes"])

                else:
                    obj = json.loads(self.request.data)
                    classes = obj["classes"]

                print repeat, classes

                if type(classes) is dict:
                    classes = classes["classes"]

                if len(classes) > 0:
                    for cls in classes:
                        repeat = int(cls["repeat"])
                        cls_request = RequestClass(cls)
                        result = cls_request.process(repeat)
                        results.append(result)

                final_name = "classes"
            else:
                pass
        except:
            print traceback.print_exc()
            if type(repeat) is not int:
                return ExceptionHandler("repeat", ExceptionType.wrong_type_for_parameter, data_type=type(repeat))
            else:
                return ExceptionHandler("generate_data", ExceptionType.other, "Something went wrong!")

        if final_name == "" or results is None:
            return ExceptionHandler("generate_data", ExceptionType.other, "Something went wrong!")

        return final_name, results

    def _get_proper_name(self):
        pass

    def _get_data_by_category(self):
        result = None
        attribute = None
        category = None
        repeat = None
        if self.method == "POST":
            try:
                if len(self.request.form.keys()) > 0:
                    data = json.loads(self.request.form)
                    attribute = data["attribute"]
                    category = data["category"]
                    repeat = data["repeat"]
                else:
                    data = json.loads(self.request.data)
                    attribute = data["attribute"]
                    category = data["category"]
                    repeat = int(data["repeat"])

                if attribute is not None and len(attribute) > 1:
                    if category is not None and category in AttributeCategories:
                        result = []
                        data_generator = FakeDataGenerator()
                        for i in range(0, repeat):
                            result.append(data_generator.get_data(category, None))
                            i += 1
                    else:
                        return ExceptionHandler("category", ExceptionType.missing_parameter)
                else:
                    return ExceptionHandler("attribute", ExceptionType.missing_parameter)
            except:
                if type(repeat) is not int:
                    return ExceptionHandler("repeat", ExceptionType.wrong_type_for_parameter, data_type=type(repeat))
                else:
                    return ExceptionHandler("get_data_by_category", ExceptionType.other, "Something went wrong!")

        return attribute, result
