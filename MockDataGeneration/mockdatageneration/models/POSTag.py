import nltk
from mockdatageneration import g
from mockdatageneration.helpers.pyenchant_spell_checker import EnchantSpellChecker

__author__ = 'maria'


class POSTagger(object):
    """
    Handles postagging, returns noun
    """
    def __init__(self):
        self.text_list = []
        self.gate_tagger = []
        self.result = {}
        self.spell_checker = EnchantSpellChecker()
        self.spell_checker.dict_exists('en')    # init spell checker with english

    def pos_tag(self, split_attribute):
        self.text_list = split_attribute
        self.gate_experimental_pos_tagging_using_model()

        return self._get_nouns()

    def gate_experimental_pos_tagging_using_model(self):
        """
            Get pos tagging results using custom tagger with the model provided by gate twitter tagger.
            Reference: https://gate.ac.uk/wiki/twitter-postagger.html
            L. Derczynski, A. Ritter, S. Clarke, and K. Bontcheva, 2013: "Twitter
            Part-of-Speech Tagging for All: Overcoming Sparse and Noisy Data". In:
            Proceedings of the International Conference on Recent Advances in Natural
            Language Processing.
        """
        default_tagger = nltk.data.load(nltk.tag._POS_TAGGER)
        train_model = g.train_model
        tagger = nltk.tag.UnigramTagger(model=train_model, backoff=default_tagger)
        # if len(self.text_list) == 1:
        #     tagger = nltk.tag.UnigramTagger(model=train_model, backoff=default_tagger)
        # elif len(self.text_list) == 2:
        #     tagger = nltk.tag.BigramTagger(model=train_model, backoff=default_tagger)
        # elif len(self.text_list) >= 3:
        #     tagger = nltk.tag.TrigramTagger(model=train_model, backoff=default_tagger)

        self.gate_tagger = tagger.tag(self.text_list)
        self.result = dict(self.gate_tagger)

        # print 'gate_tagger :::\t'+str(self.gate_tagger)

    def _get_nouns(self):
        nouns = []
        for k, v in self.result.items():
            if "NN" == v or "NNS" == v:
                g.logger.debug("{0} has errors: {1}".format(k, self.spell_checker.spell_checker_for_word(k)))
                if self.spell_checker.spell_checker_for_word(k) is None:
                    nouns.append(k)
        if len(nouns) == 0:
            for k, v in self.result.items():
                if "NNP" == v:
                    g.logger.debug("{0} has errors: {1}".format(k, self.spell_checker.spell_checker_for_word(k)))

                    if self.spell_checker.spell_checker_for_word(k) is None:
                        nouns.append(k)
        if len(nouns) == 0:
            for k, v in self.result.items():
                if "VB" in v:
                    if self.spell_checker.spell_checker_for_word(k) is None:
                        nouns.append(k)
        g.logger.debug("NOUNS for {0}:\t{1}\t{2}".format(self.text_list, nouns, str(self.result)))
        return nouns

pos_tagger = POSTagger()
# print pos_tagger.pos_tag(["alarm", "message"])
# print pos_tagger.pos_tag(["foreign", "customer"])
# print pos_tagger.pos_tag(["I", "love", "music"])
# print pos_tagger.pos_tag(["message", "alarm"])
# print pos_tagger.pos_tag(["DGEGFCG", "mesFGHTFHsage"])
# print pos_tagger.pos_tag(["customs", "declaration"])
# print pos_tagger.pos_tag(["flight", "info"])
# print pos_tagger.pos_tag(["loss", "event"])
# print pos_tagger.pos_tag(["message", "interchange"])
# print pos_tagger.pos_tag(["parcel", "status", "change"])
# print pos_tagger.pos_tag(["parcel", "status", "change", "event"])
# print pos_tagger.pos_tag(["postal", "operator"])
# print pos_tagger.pos_tag(["priority", "risk"])
# print pos_tagger.pos_tag(["alarm", "type"])
# print pos_tagger.pos_tag(["parcel"])
# print pos_tagger.pos_tag(["customs", "delcaration", "cn23"])
# print pos_tagger.pos_tag(["document", "type"])