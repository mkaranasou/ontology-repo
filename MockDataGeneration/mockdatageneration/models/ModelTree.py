import json
from nltk.corpus import wordnet_ic
from mockdatageneration import NameHelper
from mockdatageneration.models.Match import Match
import re
import requests
from nltk.corpus import wordnet as wn

# Globals
brown_ic = wordnet_ic.ic('ic-brown.dat')
semcor_ic = wordnet_ic.ic('ic-semcor.dat')

__author__ = 'm.karanasou'


class ModelClass(object):
    def __init__(self, xml_doc, parent=None):
        self.xml_doc = xml_doc
        self.name_helper = NameHelper()
        self.id = xml_doc.get("id")
        self.name = xml_doc.get("Name")
        self.preprocessed_name = self.name_helper.get_name(self.name)
        self.use_preprocessed_name = self.name_helper.has_been_split
        self.pseydoname = xml_doc.get("PseydoName")
        self.preprocessed_pseydoname = self.name_helper.get_name(self.pseydoname)
        self.use_preprocessed_pseydoname = self.name_helper.has_been_split
        self.data_type = xml_doc.get("DataType")
        self.source = xml_doc.get("Source")
        self.model = xml_doc.get("ModelName")
        self.level = xml_doc.get("Level")
        self.is_matched = False
        self.is_a = self._get_is_a()
        self.synonyms = self._get_synonyms()
        self.parent = parent
        self.numbers_in_pseydoname = re.match("\d", self.pseydoname)
        self.subclasses = []
        self.attributes = []
        self.match = Match()
        self.load(self.xml_doc)

    def load(self, xml):
        for each in xml:
            if each.tag == "Attribute":
                self.attributes.append(ModelAttribute(each, self))
            elif each.tag == "Class":
                subclass = ModelClass(each, parent=self)
                self.subclasses.append(subclass)

    def _get_is_a(self):
        url = 'http://conceptnet5.media.mit.edu/data/5.2/search?rel=/r/IsA&start=/c/en/{0}&limit=1'
        response = []
        is_a = []

        # # call rest api
        # if self.use_preprocessed_name:
        #     for each in self.preprocessed_name:
        #         response.append(requests.get(url.format(each)))
        # else:
        #     response.append(requests.get(url.format(self.name.lower())))
        #
        # for resp in response:
        #     data = json.loads(resp.text)
        #     # store end node if we have a response: start:"car" -- isA --> end:"vehicle"
        #     if data is not None and len(data["edges"]) > 0:
        #         is_a.append(data["edges"][0]["end"].replace("/c/en/", ""))
        return is_a

    def _get_synonyms(self):
        """
        Need to have relevance of synonyms to parent concept
        :return:
        """
        synsets = []
        if self.use_preprocessed_name:
            for each in self.preprocessed_name:
                synsets += wn.synsets(each, pos="n")
        else:
            synsets = wn.synsets(self.name)

        if self.use_preprocessed_pseydoname:
            for each in self.preprocessed_pseydoname:
                synsets += wn.synsets(each, pos="n")
        else:
            synsets = wn.synsets(self.pseydoname)

        results = []
        for each in synsets:
            results += [y for y in each.lemma_names()]

        return set(results)     # get unique results


class ModelAttribute(object):
    def __init__(self, xml_doc, parent):
        self.xml_doc = xml_doc
        self.name_helper = NameHelper()
        self.id = xml_doc.get("id")
        self.name = xml_doc.get("Name")
        self.preprocessed_name = self.name_helper.get_name(self.name)
        self.use_preprocessed_name = self.name_helper.has_been_split
        self.pseydoname = xml_doc.get("PseydoName")
        self.data_type = xml_doc.get("DataType")
        self.preprocessed_pseydoname = self.name_helper.get_name(self.pseydoname)
        self.use_preprocessed_pseydoname = self.name_helper.has_been_split
        self.source = xml_doc.get("Source")
        self.model = xml_doc.get("ModelName")
        self.level = xml_doc.get("Level")
        self.is_matched = False
        self.is_id = False
        self.is_a = self._get_is_a()
        self.synonyms = self._get_synonyms()
        self.parent_class = parent
        self.match = Match()
        self.numbers_in_pseydoname = re.match("\d", self.pseydoname)

    def _get_is_a(self):
        url = 'http://conceptnet5.media.mit.edu/data/5.2/search?rel=/r/IsA&start=/c/en/{0}&limit=1'
        response = []
        is_a = []

        # # call rest api
        # if self.use_preprocessed_name:
        #     for each in self.preprocessed_name:
        #         response.append(requests.get(url.format(each)))
        # else:
        #     response.append(requests.get(url.format(self.name.lower())))
        #
        # for resp in response:
        #     data = json.loads(resp.text)
        #     # store end node if we have a response: start:"car" -- isA --> end:"vehicle"
        #     if data is not None and len(data["edges"]) > 0:
        #         is_a.append(data["edges"][0]["end"].replace("/c/en/", ""))
        return is_a

    def _get_synonyms(self):
        """
        Need to have relevance of synonyms to parent concept
        :return:
        """
        synsets = []
        if self.use_preprocessed_name:
            for each in self.preprocessed_name:
                synsets += wn.synsets(each)
        else:
            synsets = wn.synsets(self.name)
        results = []
        for each in synsets:
            results += [y for y in each.lemma_names()]
        # print self.name,  ":\n", results
        return results


class TransformationModel(object):
    def __init__(self, xml_doc):
        self.xml_doc = xml_doc
        self.left_tree = None
        self.right_tree = None

    def load(self):
        if self.xml_doc is not None:
            self.left_tree = ModelClass(self.xml_doc.find(".//Trees/Left/Class"))
            self.right_tree = ModelClass(self.xml_doc.find(".//Trees/Right/Class"))

    def suggest(self):
        pass
