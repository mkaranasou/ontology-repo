import json
from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic
import urllib2
from mockdatageneration.data.attribute_categories import AttributeCategories

# Globals
brown_ic = wordnet_ic.ic('ic-brown.dat')
semcor_ic = wordnet_ic.ic('ic-semcor.dat')

__author__ = 'm.karanasou'


class SimilarityFinder(object):
    """

    """
    def __init__(self):
        self.categories = ["Human", "Address", "Animal", "Company", "Card", "Date", "Computer"]
        self.attribute_categories = list(AttributeCategories.keys())

    def find_from_wordnet(self, word_or_list):

        if type(word_or_list) is list:
            return self.find_max_for_list_from_wordnet(word_or_list)
        else:
            return self.find_max_for_word_from_wordnet(word_or_list)

    def find_max_for_word_from_wordnet(self, word):
        cls_res = []
        final_category = ""
        max_ = 0

        try:
            for each in self.categories:
                a = wn.synset('{0}.n.01'.format(word.lower()))
                b = wn.synset('{0}.n.01'.format(each.lower()))
                sim = wn.res_similarity(a, b, brown_ic)
                cls_res.append(sim)
            max_ = max(cls_res)
            final_category = self.categories[cls_res.index(max_)]
        except:
            max_ = 0
            final_category = "Unknown"
        return {"category": final_category, "value": max_}  # saves category name

    def find_max_for_list_from_wordnet(self, words):
        max_results_category = []
        max_results_value = []
        for word in words:
            cls_res = []
            final_category = ""
            max_ = 0
            try:
                for each in self.categories:
                    a = wn.synset('{0}.n.01'.format(word.lower()))
                    b = wn.synset('{0}.n.01'.format(each.lower()))
                    sim = wn.res_similarity(a, b, brown_ic)
                    cls_res.append(sim)
                max_ = max(cls_res)
                final_category = self.categories[cls_res.index(max_)]
            except:
                max_ = 0
                final_category = "Unknown"
            max_results_category.append(final_category)  # saves category name
            max_results_value.append(max_)  # saves category score

        final_max = max(max_results_value)
        category_with_max_sim = max_results_category[max_results_value.index(final_max)]

        return {"category": category_with_max_sim, "value": final_max}

    def find_from_conceptnet(self, word):
        """
        request: http://conceptnet5.media.mit.edu/data/5.2/assoc/c/en/human?filter=/c/en/person&limit=1
        response:
            {
              "similar": [
                [
                  "/c/en/person_offspring",
                  0.56173075898889224
                ]
              ],
              "terms": [
                [
                  "/c/en/human",
                  1.0
                ]
              ]
            }
        :param word:
        :return:
        """
        cls_res = []
        final_category = ""
        max_ = 0
        for each in self.categories:
            try:
                url = 'http://conceptnet5.media.mit.edu/data/5.2/assoc/c/en/{category}?filter=/c/en/{class_name}&limit=1'.format(category=each, class_name=word)
                response = urllib2.urlopen(url).read()
                result = json.loads(response.decode('utf8'))
                if len(result["similar"]) == 1:
                    cls_res.append(result["similar"][0][1])
            except:
                max_ = 0
                final_category = "Unknown"
                print "exception"
                pass
        max_ = max(cls_res)
        final_category = self.categories[cls_res.index(max_)]

        return {"category": final_category, "value": max_}  # saves category name

similarity_finder = SimilarityFinder()