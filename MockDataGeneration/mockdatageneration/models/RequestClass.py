from mockdatageneration.models.AttributeCategoryHelper import AttributeCategoryHelper
from mockdatageneration import similarity_finder, fake_data, pos_tagger
from mockdatageneration.models.NameHelper import NameHelper

__author__ = 'm.karanasou'



class RequestClass(object):
    """
        Handles all incoming requests
    """
    def __init__(self, data):
        self.data = data
        self.name_helper = NameHelper()
        self.name = self._get_name()
        self.module = data["module"] if "module" in data else ""
        self.attributes = []
        self.category = None
        self.repeat = 0
        self.similarity_finder = similarity_finder
        self.results = {}
        self.pos_tagger = pos_tagger

    def _get_name(self):
        """
        If name is in word_otherword form or wordOtherword or WordOtherword text similarity cannot be determinded,
        since name is not a noun.
        It is attempted to split name correctly and get noun or nouns to perform text similarity.
        In case of nouns, category with max similarity is returned (if noun_a has 0.5 max similarity and noun_b has 1.5 max
          similarity then category of noun_b is returned.
        :return: list that contains noun to be checked for similarity later on.
        """
        if "name" in self.data.keys():
            return self.name_helper.get_name(self.data["name"])
        return ""   # todo: Raise Exception

    def _get_attributes(self):
        if "attributes" in self.data.keys():
            for attr in self.data["attributes"]:
                attr_req = RequestAttribute(attr, self)
                attr_req.process(self.repeat)
                self.attributes.append({attr["name"]: attr_req.results})

    def _get_category(self):
        """
        Categorizes request class using name and text similarity.
        This is required to get
        :return:
        """
        self.category = self.similarity_finder.find_from_wordnet(self.name)

    def process(self, repeat):
        self.repeat = repeat
        self._get_category()
        self._get_attributes()

        self.results["name"] = self.data["name"]
        self.results["module"] = self.module
        self.results["attributes"] = self.attributes

        return self.results


class RequestAttribute(object):
    """

    """
    def __init__(self, data, parent_cls):
        self.data = data
        self.name_helper = NameHelper()
        self.name = self._get_name()
        self.data_type = data["datatype"]
        self.parent_cls = parent_cls
        self.category = None
        self.similarity_finder = similarity_finder
        self.repeat = 0
        self.results = []

    def _get_category(self):
        self.category = AttributeCategoryHelper().get_attribute_category(self.parent_cls.category["category"], self.name)
        if self.category is None:
            # self.category = self.similarity_finder.find_from_wordnet(self.name, True)["category"].lower()
            # if self.category == "unknown":
                self.category = None

    def _get_data(self):
        print "attr {0} belongs to {1}".format(self.name, self.category)
        for times in range(0, self.repeat):
            self.results.append(fake_data.get_data(self.category, self.data_type))

    def _get_name(self):
        """
        If name is in word_otherword form or wordOtherword or WordOtherword text similarity cannot be determinded,
        since name is not a noun.
        It is attempted to split name correctly and get noun or nouns to perform text similarity.
        In case of nouns, category with max similarity is returned (if noun_a has 0.5 max similarity and noun_b has 1.5 max
          similarity then category of noun_b is returned.
        :return: list that contains noun to be checked for similarity later on.
        """
        if "name" in self.data.keys():
            return self.name_helper.get_name(self.data["name"])
        return ""   # todo: Raise Exception

    def process(self, repeat):
        self.repeat = repeat
        self._get_category()
        self._get_data()


