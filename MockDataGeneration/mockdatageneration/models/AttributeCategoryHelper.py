from mockdatageneration.data.class_categories_per_attribute_categories import Categories
from mockdatageneration.data.attribute_categories import AttributeCategories

__author__ = 'm.karanasou'


class AttributeCategoryHelper(object):
    def __init__(self):
        self.categories = Categories

    def get_attribute_category(self, parent_category, attribute):
        final_category = None
        temp_attr = attribute

        if type(attribute) is list:
            temp_attr = attribute[0]

        if parent_category in self.categories.keys():
            possible_categories = sorted(self.categories[parent_category], key=len)

            for subcategory in possible_categories:
                if subcategory in AttributeCategories.keys():
                    if temp_attr in subcategory:
                        final_category = subcategory
                        break
                    if temp_attr in AttributeCategories[subcategory]:
                        final_category = subcategory
                        break

            if final_category is None:
                for subcategory in possible_categories:
                    if subcategory in AttributeCategories.keys():
                        for synonym in AttributeCategories[subcategory]:
                            if temp_attr in synonym:
                                final_category = subcategory
                                break

        return final_category