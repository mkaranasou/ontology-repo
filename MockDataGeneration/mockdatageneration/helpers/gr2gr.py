# -*- coding: utf-8 -*-

# el2gr
# A python script to transliterate
# greek (utf-8 or ISO-8859-7) to "greeklish".
# Of course uses my own preference for greeklish.
# 
# usage:
# pipe input into STDIN, read input from STDOUT
# use --unicode, -u, --utf8, --utf-8 if your input is in UTF-8
# otherwise it will assume iso-8859-7
# characters not in the iso-8859-7 range will be replaced by '?'

unioptions = ['--unicode', '-u', '--utf8', '--utf-8']

import string
import sys

if len(sys.argv) > 1:
    option = sys.argv[1]
else:
    option = '-iso'

input_string = "τεστ"

# from_chars = 'áâãäåæçèéêëìíîïðñóòôõö÷øùÜÝÞßúÀüýûàþÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÓÔÕÖ×ØÙ¶¸¹ºÚ¼¾Û¿'

from_chars = 'áâãäåæçèéêëìíîïðñóòôõö÷øùÜÝÞßúÀüýûàþÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÓÔÕÖ×ØÙ¶¸¹ºÚ¼¾Û¿'
to_chars =   'abgdezh8iklmn3oprsstufxywaehiiiouuuwABGDEZH8IKLMNJOPRSTYFXCWAEHIIOUUW'

if option in unioptions:
    input_string = input_string.decode('utf-8')
    input_string = input_string.encode('iso-8859-7', 'replace')
print(len(from_chars), len(to_chars))
# translation_table = string.maketrans(from_chars, to_chars)
translation_table = from_chars.translate(to_chars)
input_string = translation_table.translate(input_string)
print input_string