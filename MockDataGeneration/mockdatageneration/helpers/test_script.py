# -*- coding: utf-8 -*-
from collections import Counter
import json
import datetime
from nltk.corpus import wordnet
from itertools import product
from mockdatageneration import NameHelper
from mockdatageneration.helpers.pyenchant_spell_checker import EnchantSpellChecker
from mockdatageneration.models.Classifier import Classifier
from mockdatageneration.models.ItemModel import Item, InternalItem
from py2neo import Graph
from py2neo.packages.httpstream import http
# http://stackoverflow.com/questions/27078352/py2neo-2-0-errorhttpstream-socketerror-timed-out
# from rdflib import RDF
http.socket_timeout = 9999
import re
import requests
from mockdatageneration.data.class_categories_per_attribute_categories import Categories
from mockdatageneration.data.domain_model import domain_model
from mockdatageneration.helpers.globals import g
from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic
brown_ic = wordnet_ic.ic('ic-brown.dat')
semcor_ic = wordnet_ic.ic('ic-semcor.dat')

__author__ = 'm.karanasou'


conversion_chars = {
        u'α': 'a',
        u'β': 'b',
        u'γ': 'c',
        u'δ': 'd',
        u'ε': 'e',
        u'ζ': 'z',
        u'η': 'i',
        u'θ': 'th',
        u'ι': 'i',
        u'κ': 'k',
        u'λ': 'l',
        u'μ': 'm',
        u'ν': 'n',
        u'ξ': 'ks',
        u'ο': 'o',
        u'π': 'p',
        u'ρ': 'r',
        u'σ': 's',
        u'τ': 't',
        u'υ': 'y',
        u'φ': 'f',
        u'χ': 'x',
        u'ψ': 'ps',
        u'ω': 'w',
    }


def categories_test():
    categories_per_synonym = {}
    for k, v in Categories.items():
        for att_cat in v:
            faker_func = "fake." + att_cat + "()"
            syns = wordnet.synsets(att_cat, 'n')
            categories_per_synonym[att_cat] = []
            for s in syns:
                print s
                for l in s.lemmas():
                    categories_per_synonym[att_cat].append(l._name.encode('ascii'))
    print categories_per_synonym


def sim_test():

    # categories = ["human", "address", "animal", "company", "Bank", "date", "computer", "internet"]
    categories_per_synonym = {}
    for category in Categories.keys():
        syns = wordnet.synsets(category, 'n')
        categories_per_synonym[category] = []
        for s in syns:
            print s
            for l in s.lemmas():
                categories_per_synonym[category].append(l._name.encode('ascii'))

    print categories_per_synonym

    categories_per_attribute = {}
    for k, v in Categories.items():
        for item in v:
            syns = wordnet.synsets(item, 'n')
            categories_per_attribute[k] = []
            for s in syns:
                print s
                for l in s.lemmas():
                    categories_per_attribute[k].append(l._name.encode('ascii'))

    print categories_per_attribute


def post_get_by_category():
    payload = {
               "attribute": "surname",
               "category": "last_name",
               "repeat": 10
              }

    url = 'http://localhost:8082/api/1.0/data/get_data_by_category'
    # headers = {'content-type': 'application/x-www-form-urlencoded'}
    headers = {'content-type': 'application/json'}

    response = requests.post(url, data=json.dumps(payload), headers=headers)

    print response.content


def post_test():
    payload = {
                "classes": [{
                    "name": "Customer",
                    "repeat": 10,
                    "module": "DomainModel",
                    "attributes": [
                    {
                      "name": "name",
                      "datatype": "string"
                    },
                    {
                      "name": "surname",
                      "datatype": "string"
                    },
                    {
                      "name": "age",
                      "datatype": "int"
                    },
                    {
                      "name": "mainAddress",
                      "datatype": "Address"
                    },
                    {
                      "name": "alternativeAddress",
                      "datatype": "Address"
                    }, {
                      "name": "addresses",
                      "datatype": "Collection"
                    }]
                    }, {
                            "name": "Address",
                            "repeat": 5,
                            "module": "DomainModel",
                            "attributes": [{
                              "name": "street",
                              "datatype": "string"
                            }, {
                              "name": "number",
                              "datatype": "int"
                            }]
                    }]
                }

    url = 'http://localhost:8082/api/1.0/data/get_data'
    # headers = {'content-type': 'application/x-www-form-urlencoded'}
    headers = {'content-type': 'application/json'}

    response = requests.post(url, data=json.dumps(payload), headers=headers)

    print response.content


def test_split():
    print re.split(r'([A-Z][a-z]*\d*)', "mainAddress")
    print re.split(r'([A-Z][a-z]*\d*)', "testdata")
    # print "Address_".split("_")


def test_neo4j():
    from requests.auth import HTTPBasicAuth
    # from base64 import b64encode
    # This sets up the https connection
    # data = requests.get('http://localhost:7474/db/data/', auth=HTTPBasicAuth('neo4j', '!291119851$Mk'))
    # query = {"query": "MATCH (n)WHERE n.name =~ \"en/.*\" RETURN n LIMIT 10"}
    query = {"label": "test"}
    headers = {'content-type': 'application/json'}
    q = requests.get('http://localhost:7474/service/graphify/similar/', auth=HTTPBasicAuth('neo4j', '123456'), data=json.dumps(query),  headers=headers)

    print q.content


def test_most_frequent_words_in_domain():
    import re
    from collections import Counter
    passage = str(domain_model)
    # with open('../data/DomainModel/DomainModel/complete_protocol_domain_model.zn') as f:
    #     passage = f.read()

    words = re.findall(r'\w+', passage)

    cap_words = [word.upper() for word in words]

    word_counts = Counter(cap_words)

    for k,v in word_counts.items():
        if v <= 10:
            print k, " #", v


def test_name_split():
    from collections import Counter
    graph = Graph("http://neo4j:!291119851$Mk@localhost:7474/db/data/")

    name_helper = NameHelper()
    cls_results = []
    attr_results = []
    for cls in domain_model["classes"]:
        cls_results.append(name_helper.get_name(cls["name"]))

        for attr in cls["attributes"]:
            attr_results.append(name_helper.get_name(attr["name"]))

    words = re.findall(r'\w+', str(cls_results))

    cap_words = [word.upper() for word in words]

    word_counts = Counter(cap_words)

    print "=================== CLASSES ================"
    for k, v in word_counts.items():
        if v > 1:
            isa = graph.cypher.execute('MATCH (n)-[r:IsA]->(m) WHERE n.id="/c/en/{0}" RETURN m.id LIMIT 1;'.format(k.lower()))
            print k, " #", v, "IsA: ", isa

    words = re.findall(r'\w+', str(attr_results))

    cap_words = [word.upper() for word in words]

    word_counts = Counter(cap_words)

    print "=================== ATTRIBUTES ================"
    for k, v in word_counts.items():
        if v > 1:
            isa = graph.cypher.execute('MATCH (n)-[r:IsA]->(m) WHERE n.id="/c/en/{0}" RETURN m.id LIMIT 1;'.format(k.lower()))
            print k, " #", v, "IsA: ", isa


def test_coca_cola_concept(xml_1):
    import xml.etree.ElementTree as ET
    internal_class_names = []
    internal_class_attribute_names = []
    external_class_names = []
    external_class_attribute_names = []
    name_helper = NameHelper()
    tree = ET.parse(xml_1)

    left_tree = tree.find(".//Trees/Left/Class")
    right_tree = tree.find(".//Trees/Right/Class")

    internal_classes = left_tree.findall(".//Class")
    external_classes = right_tree.findall(".//Class")

    print "internal:"
    print "=========================================="
    for each in internal_classes:
        print(each.get("Name"), each.get("PseydoName"))
        internal_class_names.append(name_helper.get_name(each.get("Name")))
        internal_attributes = each.findall(".//Attribute")
        if internal_attributes is not None:
            for attr in internal_attributes:
                internal_class_attribute_names.append(name_helper.get_name(attr.get("Name")))

    print "external:"
    print "=========================================="
    for each in external_classes:
        print(each.get("Name"), each.get("PseydoName"))
        external_class_names.append(name_helper.get_name(each.get("Name")))
        external_attributes = each.findall(".//Attribute")
        if external_attributes is not None:
            for attr in external_attributes:
                external_class_attribute_names.append(name_helper.get_name(attr.get("Name")))

    for each in internal_class_names:
        print "working on ", each
        if each in external_class_names:
            print each, " - matches ->", external_class_names[external_class_names.index(each)]
        else:
            synonyms = wordnet.synsets("{0}.{1}.01".format(each, "n"))
            for synonym in synonyms:
                if synonym in external_class_names:
                    print each, " matches ", synonym

    for each in internal_class_attribute_names:
        match = None
        print "ATTRIBUTES: working on ", each
        for attr in each:
            for ext in external_class_attribute_names:
                if attr in ext:
                    match = ext
                    print "match", match
                    break
            if match is not None:
                print each, " - matches ->", external_class_attribute_names[external_class_attribute_names.index(match)]
            else:
                synonyms = wordnet.synsets("{0}.{1}.01".format(each, "n"))
                for synonym in synonyms:
                    for ext in external_class_attribute_names:
                        if synonym in ext:
                            match = ext
                            print "match", match
                            break
                    if match is not None:
                        print each, " matches ", synonym


def test_rdf_parser():
    from rdflib import Graph

    g = Graph()
    g.parse("http://ontology.tno.nl/logico.ttl", format="n3")
    # len(g)
    # print g.triples()
    # import pprint
    # for stmt in g:
    #     # pprint.pprint(stmt)
    #     print "id:", stmt[0], "what:", stmt[1], "?", stmt[2]
    # for s,p,o in g.triples((None,  RDF.type, None)):
    #     print "%s is a %s" % (s, o)
    # for each in g.objects():
    #     print each

    print(g.serialize(format='json-ld', indent=4))


def test_synsets():
    from nltk.corpus import wordnet as wn
    for ss in wn.synsets('love'):
        print ss.definition()
        print ss.lemma_names()
        print


def split_all_lower_in_naive_way(word):
    words = []
    spell_checker = EnchantSpellChecker()
    tmp = ""
    count = 0
    for char in range(0, len(word)):
        tmp = word[::-1][char] + tmp
        print tmp
        count += 1
        if count > 1 and spell_checker.spell_checker_for_word(tmp)is None:
            words.append(tmp)
            tmp = ""
            count = 0
    print spell_checker.correct_word(tmp)
    print(word, words[::-1])


def infer_spaces(s):

    # source:https://stackoverflow.com/questions/8870261/how-to-split-text-without-spaces-into-list-of-words/11642687#11642687

    from math import log

    # Build a cost dictionary, assuming Zipf's law and cost = -math.log(probability).
    words = open("../data/words-by-frequency.txt").read().split()
    wordcost = dict((k, log((i+1)*log(len(words)))) for i, k in enumerate(words))
    maxword = max(len(x) for x in words)
    """Uses dynamic programming to infer the location of spaces in a string
    without spaces."""

    # Find the best match for the i first characters, assuming cost has
    # been built for the i-1 first characters.
    # Returns a pair (match_cost, match_length).
    def best_match(i):
        candidates = enumerate(reversed(cost[max(0, i-maxword):i]))
        return min((c + wordcost.get(s[i-k-1:i], 9e999), k+1) for k, c in candidates)

    # Build the cost array.
    cost = [0]
    for i in range(1,len(s)+1):
        c,k = best_match(i)
        cost.append(c)

    # Backtrack to recover the minimal-cost string.
    out = []
    i = len(s)
    while i > 0:
        c, k = best_match(i)
        assert c == cost[i]
        out.append(s[i-k:i])
        i -= k

    return " ".join(reversed(out))


def find_words(instring, prefix = '', words = None):
    if not instring:
        return []
    if words is None:
        words = set()
        with open('../data/words-by-frequency.txt') as f:
            for line in f:
                words.add(line.strip())
    if (not prefix) and (instring in words):
        return [instring]
    prefix, suffix = prefix + instring[0], instring[1:]
    solutions = []
    # Case 1: prefix in solution
    if prefix in words:
        try:
            solutions.append([prefix] + find_words(suffix, '', words))
        except ValueError:
            pass
    # Case 2: prefix not in solution
    try:
        solutions.append(find_words(suffix, prefix, words))
    except ValueError:
        pass
    if solutions:
        return sorted(solutions,
                      key=lambda solution: [len(word) for word in solution],
                      reverse=True)[0]
    else:
        raise ValueError('no solution')


def test_similarity(word_a, word_b, pos="n"):
    # semcor_ic = wordnet_ic.ic('ic-semcor.dat')
    print word_a, word_b
    a = wn.synset('{0}.{1}.01'.format(word_a.lower(), pos))
    b = wn.synset('{0}.{1}.01'.format(word_b.lower(), pos))
    print "res", wn.res_similarity(a, b, brown_ic)
    print "path", wn.path_similarity(a, b)
    print "wup", wn.wup_similarity(a, b)
    print "lin", wn.lin_similarity(a, b, brown_ic)


def test_similarity_max(word_a, word_b):
    ss1 = wn.synsets(word_a, pos="n")
    ss2 = wn.synsets(word_b, pos="n")
    print max(s1.res_similarity(s2, semcor_ic) for (s1, s2) in product(ss1, ss2))


def import_cocacola_items():

    d = "../data/cocacola_concept/DistributorItems.txt"
    s = "../data/cocacola_concept/SupplierItems_with_categories.txt"
    name_helper = NameHelper(False, False)
    result_d = []
    result_d_items = []
    result_s_items = []
    result_s = []

    with open(d, "rb") as distributor_items:
        for line in distributor_items.readlines():
            line = line.strip("\r\n")
            result_d_items.append(convert_greek_to_greeklish(unicode(line, "utf-8")))
            for x in name_helper.get_name(line):
                if x != "" and len(x) > 2:
                    result_d.append(x)

    # for each in result_d:
    #     print each

    with open(s, "rb") as supplier_items:
        for line in supplier_items.readlines():
            line = line.strip("\r\n")
            item = InternalItem(line)
            result_s_items.append(item)
            # for x in name_helper.get_name(line):
            #     if x != "" and len(x) > 2:
            #         result_s.append(x)

    BRANDS = []
    CATEGORIES = []
    items_per_category = {}

    for each in result_s_items:
        brand_lower = each.brand.lower()
        if brand_lower not in BRANDS:
            BRANDS.append(brand_lower)
        if brand_lower not in items_per_category:
            items_per_category[brand_lower] = []
        items_per_category[brand_lower].append(each)

        if each.category.lower() not in CATEGORIES:
            CATEGORIES.append(each.category.lower())

    RESULTS = {}
    found = 0
    for each in result_d_items:
        tpt = False
        for brand in BRANDS:
            if brand in each.lower():
                RESULTS[each.lower()] = items_per_category[brand]
                # print each.lower().encode("utf8"), " belongs to ", brand
                tpt = True
                break
        if tpt:
            found += 1

    found_b = 0
    for k, v in RESULTS.items():
        # print k.encode("utf8")
        res = ""
        for each in v:
            c = 0
            # print each.item_quantity, each.how_many,  k.encode("utf8")
            for other in each.others:
                if len(re.findall(other.lower(), k)) > 0:
                    c += 1
            if each.quantity_type.lower() in k:
                c += 1

            if (each.item_quantity in k and each.how_many in k) and c > 0:
                res += k.encode("utf8") + "\t=======>"
                res += each.item_quantity + "\t"
                res += each.how_many + "\t"
                res += each.brand + "\t"
                res += " ".join(each.others) + "\t"
                found_b += 1
                print res
                break

    print found, "out of ", len(result_d_items), found_b

    # for each in result_s:
    #     print each

    # cap_words = [word.upper() for word in result_s_items]
    # word_counts = Counter(cap_words)
    # for k, v in word_counts.items():
    #     if v > 1:
    #         print k, v

    # for each in result_d:
    #     for word in each:
    #         for suppl in result_s:
    #             if word in suppl:
    #                 print "FOUND {0} -- > {1}".format(word, suppl)
    #                 break


def convert_greek_to_greeklish(sentence):
    new_sentence = ""
    for char in sentence:
        # print char.lower().encode('cp1252')  # repr()
        if char.lower() in conversion_chars:
            new_sentence += conversion_chars[char.lower()]
            # print "found!", conversion_chars[char.lower()]
        else:
            new_sentence += char.lower()
    # print new_sentence.encode('cp1252')
    return new_sentence


def classification_simple_example(cls_type, vec_type):
    suppliers = []
    distributors = []
    unmatched = []
    with open('../data/cocacola_concept/final_data_set.txt', "rb") as example:
        for line in example.readlines():
            split_line = line.replace("\r\n", "").split("\t")
            distributors.append(split_line[0])
            suppliers.append(split_line[1])

    with open('../data/cocacola_concept/unmatched.txt', "r") as example:
        for line in example.readlines():
            split_line = line.replace("\r\n", "").split("\t")
            unmatched.append(split_line[2].replace("\n", ""))

    len_ = len(suppliers)
    train_set_size = 3 * (len_/4)

    x_train = suppliers
    y_train = distributors
    # x_test = distributors[train_set_size:]
    x_test = unmatched
    # y_test = suppliers[train_set_size:]

    cls = Classifier(cls_type, vec_type)
    cls.set_x_train(x_train)
    cls.set_y_train(y_train)
    cls.set_x_trial(x_test)

    cls.train()
    predictions = cls.predict()
    with open('../data/cocacola_concept/results/results{0}_{1}.txt'.format(datetime.date.today(), cls.cls_type), "wb") as results_file:
        for i in range(0, len(predictions)):
            results_file.write(x_test[i] + "\t" + predictions[i] + "\r\n")


def clean_up():
    supp = {}
    distr = {}

    matched = {}
    not_matched = {}

    # get all items of supplier
    with open("../data/cocacola_concept/SupplierItems_with_categories.txt", "rb") as given:
        for line in given.readlines():
            split_line = line.strip("\r\n").split("\t")
            supp[split_line[2]] = split_line[3]

    # get all matched items of distributor
    with open("../data/cocacola_concept/matched.txt", "rb") as matched_distr:
        for line in matched_distr.readlines():
            split_line = line.strip("\r\n").split("\t")
            distr[split_line[0]] = split_line[2]

    # aggregate matched distr items by sku
    for k, v in distr.items():
        if k not in matched:
            matched[k] = []

        matched[k].append(v)

    # find supplier items that are not matched
    for k, v in supp.items():
        if k not in matched:
            not_matched[k] = v

    with open("../data/cocacola_concept/missing_matches.txt", "wb") as missing_matches:
        for k, v in not_matched.items():
            missing_matches.write(k + "\t" + v + "\r\n")


def update_missing_matches():
    pairs = []
    with open("../data/cocacola_concept/missing_matches.txt", "rb") as given:
        for line in given.readlines():
            split_line = line.strip("\r\n").split("\t")
            pairs.append(line.strip("\r\n") + "\t" + english_to_greek(split_line[1]) + "\r\n")

    with open("../data/cocacola_concept/missing_matches.txt", "wb") as given1:
        for pair in pairs:
            given1.write(pair)


def english_to_greek(line):
    english = u'abcdefghijklmnopqrstuvwxyz'.encode("utf8")
    greek = u'αβκδεφγχιγκλμνοπκρστυηβωχυζ'.encode("utf8")

    processed_line = ""
    for char in line:
        if char.lower() in english:
            processed_line += greek[english.index(char.lower())]
        else:
            processed_line += char
    return processed_line


def gather_matched():
    distr = {}
    supp = {}
    matched = {}

    # get all items of supplier
    with open("../data/cocacola_concept/SupplierItems_with_categories.txt", "rb") as given:
        for line in given.readlines():
            split_line = line.strip("\r\n").split("\t")
            supp[split_line[2]] = split_line[3]

    # get all matched items of distributor
    with open("../data/cocacola_concept/matched.txt", "rb") as matched_distr:
        for line in matched_distr.readlines():
            split_line = line.strip("\r\n").split("\t")
            if split_line[0] not in distr:
                distr[split_line[0]] = []
            distr[split_line[0]].append(split_line[2])            # sku = [descr, ...]

    # aggregate matched distr items by sku
    for k, v in distr.items():
        if supp[k] not in matched:
            matched[supp[k]] = []
        for each in v:
            matched[supp[k]].append(each)

    missing_matches = []
    with open("../data/cocacola_concept/artificial_matching.txt", "rb") as given:
        for line in given.readlines():
            split_line = line.strip("\r\n").split("\t")
            missing_matches.append(split_line[0] + "\t" + split_line[1])

    with open("../data/cocacola_concept/final_data_set.txt", "wb") as final:
        for k, v in matched.items():
            for line in v:
                # print line, v
                final.write(k + "\t" + line + "\r\n")

        for each in missing_matches:
            final.write(each + "\r\n")


def parse_routes():
    lines = []
    q = "INSERT INTO AirlineRoutes VALUES ( {0} )\r\n"
    with open("../data/EuTravel/routes.dat",  "rb") as route_file:
        for line in route_file.readlines():
            split_line = line.replace("\r\n", "").split(",")
            lines.append(split_line)
    with open("../data/EuTravel/routes.txt",  "wb") as route_file_out:
        for line in lines:
            row = ""
            row += "'{0}'".format(line[0]) if line[0] != "" else "NULL"
            row += ","
            row += line[1].replace("\\N", "NULL") if line[1] != "" else "NULL"
            row += ","
            row += "'{0}'".format(line[2]) if line[2] != "" else "NULL"
            row += ","
            row += line[3].replace("\\N", "NULL") if line[3] != "" else "NULL"
            row += ","
            row += "'{0}'".format(line[4]) if line[4] != "" else "NULL"
            row += ","
            row += line[5].replace("\\N", "NULL") if line[5] != "" else "NULL"
            row += ","
            row += "'{0}'".format(line[6]) if line[6] != "" else "NULL"
            row += ","
            row += line[7].replace("\\N", "NULL") if line[7] != "" else "NULL"
            row += ","
            row += "'{0}'".format(line[8]) if line[8] != "" else "NULL"
            route_file_out.write(q.format(row))


test_similarity('capacity', 'latitude')
test_similarity('capacity', 'longitude')
test_similarity('latitude', 'latitude')
test_similarity('coordinate', 'latitude')
test_similarity('latitude', 'longitude')
test_similarity('longitude', 'longitude')
test_similarity('longitude', 'geography')
test_similarity('name', 'surname')
test_similarity('name', 'first_name')
# test_similarity('longitude', 'geographic')
test_similarity('object', 'object')
test_similarity('nickel', 'dime')
test_similarity('nickel', 'coin')
test_similarity('dime', 'coin')
# test_similarity('route', 'connection')
# test_similarity_max('route', 'bus')
# test_similarity_max('connection', 'bus')
# test_similarity_max('route', 'connection')
# parse_routes()
# clean_up()
# update_missing_matches()
# gather_matched()

# classification_simple_example(g.CLASSIFIER_TYPE.SVMStandalone, g.VECTORIZER.Count)

