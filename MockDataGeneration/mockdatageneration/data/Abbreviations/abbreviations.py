__author__ = 'm.karanasou'

def parse_abbreviations():
    with open('abbreviations.csv', 'rb') as abbrev_file:
        for line in abbrev_file.readlines():
            split_line = line.split(",")
            term = split_line[0].replace(".", "")
            deployed_term = split_line[1].replace("\n", "")\
                                        .replace("\"", "")\
                                        .replace("(in the Bible) ", "")\
                                        .replace("(s)", "")\
                                        .replace("(n)", "")\
                                        .replace("(ly)", "")\
                                        .replace(" (to)", "")\
                                        .replace(" (dialect)", "")\
                                        .replace(" (with)", "")\
                                        .replace("(al)", "")\
                                        .replace(" (of)", "")\
                                        .replace("(in dates) ", "")\
                                        .replace("(in the Apocrypha) ", "")
            print term, deployed_term

# parse_abbreviations()
