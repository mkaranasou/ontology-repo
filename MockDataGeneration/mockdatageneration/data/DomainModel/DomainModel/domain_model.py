domain_model = '''"classes": [{
"name" : "    Entity BusinessReport 
", 
{ "attributes" :[ {
	"name": "DateFrom",
 "datatype": "DateTime"
}, {
	"name": "DateTo",
 "datatype": "DateTime"
},]},
"name" : "TotalDocumentsInfo  
", 
{ "attributes" :[ {
	"name": "TotalDocumentsCount",
 "datatype": "int"
},]},
"name" : "IncomingDocumentsInfo  
", 
{ "attributes" :[ {
	"name": "TotalCount",
 "datatype": "int"
}, {
	"name": "TotalPercentage",
 "datatype": "decimal"
}, {
	"name": "InfoCount",
 "datatype": "int"
}, {
	"name": "InfoPercentage",
 "datatype": "decimal"
}, {
	"name": "ActionCount",
 "datatype": "int"
}, {
	"name": "ActionPercentage",
 "datatype": "decimal"
}, {
	"name": "ActionPendingCount",
 "datatype": "int"
}, {
	"name": "ActionPendingPercentage",
 "datatype": "decimal"
}, {
	"name": "ActionCompletedCount",
 "datatype": "int"
}, {
	"name": "ActionCompletedPercentage",
 "datatype": "decimal"
},]},
"name" : "OutgoingDocumentsInfo  
", 
{ "attributes" :[ {
	"name": "TotalCount",
 "datatype": "int"
}, {
	"name": "TotalPercentage",
 "datatype": "decimal"
}, {
	"name": "UnderApprovalCount",
 "datatype": "int"
}, {
	"name": "UnderApprovalPercentage",
 "datatype": "decimal"
}, {
	"name": "UnderApprovalViolationsCount",
 "datatype": "int"
}, {
	"name": "UnderApprovalViolationsPercentage",
 "datatype": "decimal"
}, {
	"name": "ForPhysicalSignaturesCount",
 "datatype": "int"
}, {
	"name": "ForPhysicalSignaturesPercentage",
 "datatype": "decimal"
}, {
	"name": "ForPhysicalSignaturesCountViolationsCount",
 "datatype": "int"
}, {
	"name": "ForPhysicalSignaturesCountViolationsPercentage",
 "datatype": "decimal"
}, {
	"name": "AtReceptionCount",
 "datatype": "int"
}, {
	"name": "AtReceptionPercentage",
 "datatype": "decimal"
}, {
	"name": "AtReceptionViolationsCount",
 "datatype": "int"
}, {
	"name": "AtReceptionViolationsPercentage",
 "datatype": "decimal"
}, {
	"name": "CompletedCount",
 "datatype": "int"
}, {
	"name": "CompletedPercentage",
 "datatype": "decimal"
}, {
	"name": "CompletedViolationsCount",
 "datatype": "int"
}, {
	"name": "CompletedViolationsPercentage",
 "datatype": "decimal"
},]},
"name" : "TotalDocumentsCompany  
", 
{ "attributes" :[ {
	"name": "TotalCount",
 "datatype": "int"
}, {
	"name": "TotalPercentage",
 "datatype": "decimal"
},]},
"name" : "CodingCategory  
", 
{ "attributes" :[ {
	"name": "CodingLevel",
 "datatype": "int"
}, {
	"name": "Code",
 "datatype": "string"
}, {
	"name": "Name",
 "datatype": "string"
}, {
	"name": "FullName",
 "datatype": "string"
}, {
	"name": "Description",
 "datatype": "string"
},]},
"name" : "Company  
", 
{ "attributes" :[ {
	"name": "CompanyName",
 "datatype": "string"
}, {
	"name": "CompanyDescription",
 "datatype": "string"
},]},
"name" : "Dashboard  
", 
{ "attributes" :[]},
"name" : "Notification  
", 
{ "attributes" :[ {
	"name": "ProtocolNo",
 "datatype": "string"
}, {
	"name": "DocumentType",
 "datatype": "string"
}, {
	"name": "URL",
 "datatype": "string"
}, {
	"name": "Remarks",
 "datatype": "string"
},]},
"name" : "Email  
", 
{ "attributes" :[ {
	"name": "DateSent",
 "datatype": "DateTime"
}, {
	"name": "Subject",
 "datatype": "string"
}, {
	"name": "Sender",
 "datatype": "string"
}, {
	"name": "BodyText",
 "datatype": "string"
},]},
"name" : "Recipient  
", 
{ "attributes" :[ {
	"name": "RecipientName",
 "datatype": "string"
}, {
	"name": "RecipientAddress",
 "datatype": "string"
}, {
	"name": "IsApplicationUser",
 "datatype": "bool"
},]},
"name" : "Attachment  
", 
{ "attributes" :[ {
	"name": "AttachedDocFileName",
 "datatype": "string"
},]},
"name" : "Employee  
", 
{ "attributes" :[ {
	"name": "RefNo",
 "datatype": "string"
}, {
	"name": "FirstName",
 "datatype": "string"
}, {
	"name": "LastName",
 "datatype": "string"
}, {
	"name": "FullName",
 "datatype": "string"
}, {
	"name": "FirstNameGR",
 "datatype": "string"
}, {
	"name": "LastNameGR",
 "datatype": "string"
}, {
	"name": "Roles",
 "datatype": "string"
}, {
	"name": "OrgUnitsWithDivision",
 "datatype": "string"
},]},
"name" : "EmployeeOrgUnitDetail  
", 
{ "attributes" :[]},
"name" : "DummyAppUser  
", 
{ "attributes" :[ {
	"name": "Id",
 "datatype": "int"
},]},
"name" : "IncomingDocumentForm  
", 
{ "attributes" :[ {
	"name": "Sender",
 "datatype": "string"
}, {
	"name": "DocumentDate",
 "datatype": "DateTime"
}, {
	"name": "DateEntered",
 "datatype": "DateTime"
}, {
	"name": "EnteredByUserId",
 "datatype": "string"
}, {
	"name": "Technical_GeographicalArea",
 "datatype": "string"
}, {
	"name": "Technical_Type",
 "datatype": "string"
}, {
	"name": "Technical_FromKmSpot",
 "datatype": "string"
}, {
	"name": "Technical_ToKmSpot",
 "datatype": "string"
}, {
	"name": "CodeLevel6Text",
 "datatype": "string"
}, {
	"name": "Coding",
 "datatype": "string"
}, {
	"name": "OnM_GeographicalArea",
 "datatype": "string"
}, {
	"name": "OnM_Type",
 "datatype": "string"
}, {
	"name": "OnM_FromKmSpot",
 "datatype": "string"
}, {
	"name": "OnM_ToKmSpot",
 "datatype": "string"
}, {
	"name": "Accounting_AFM",
 "datatype": "string"
}, {
	"name": "Accounting_ResponsibleAccountant",
 "datatype": "string"
}, {
	"name": "Accounting_PaymentDueDate",
 "datatype": "DateTime"
}, {
	"name": "Accounting_PaymentApproval",
 "datatype": "string"
}, {
	"name": "Financial_BankAccountNo",
 "datatype": "string"
}, {
	"name": "Financial_StartingDatePeriod",
 "datatype": "DateTime"
}, {
	"name": "Financial_EndingDatePeriod",
 "datatype": "DateTime"
}, {
	"name": "TollControl_AFM",
 "datatype": "string"
}, {
	"name": "TollControl_ResponsibleAccountant",
 "datatype": "string"
}, {
	"name": "TollControl_PaymentDueDate",
 "datatype": "DateTime"
}, {
	"name": "TollControl_PaymentApproval",
 "datatype": "string"
}, {
	"name": "IT_TollSystems_Task",
 "datatype": "string"
}, {
	"name": "IT_TollSystems_DateOverdue",
 "datatype": "DateTime"
}, {
	"name": "HR_Task",
 "datatype": "string"
}, {
	"name": "HR_DateOverdue",
 "datatype": "DateTime"
}, {
	"name": "Legal_Task",
 "datatype": "string"
}, {
	"name": "Legal_DateOverdue",
 "datatype": "DateTime"
}, {
	"name": "PR_Task",
 "datatype": "string"
}, {
	"name": "PR_DateOverdue",
 "datatype": "DateTime"
}, {
	"name": "QMS_Task",
 "datatype": "string"
}, {
	"name": "QMS_DateOverdue",
 "datatype": "DateTime"
}, {
	"name": "NotificationText",
 "datatype": "string"
}, {
	"name": "Subject",
 "datatype": "string"
}, {
	"name": "Relevant1",
 "datatype": "string"
}, {
	"name": "Relevant2",
 "datatype": "string"
}, {
	"name": "Relevant",
 "datatype": "string"
}, {
	"name": "AttachedDocuments",
 "datatype": "string"
}, {
	"name": "LastModifiedByUserId",
 "datatype": "string"
}, {
	"name": "DateLastModified",
 "datatype": "DateTime"
}, {
	"name": "ReferenceNumber",
 "datatype": "string"
}, {
	"name": "Type",
 "datatype": "string"
}, {
	"name": "Status",
 "datatype": "string"
}, {
	"name": "ParentIncomingDocEnteredByUserId",
 "datatype": "string"
}, {
	"name": "ParentIncomingDocEnteredDateTime",
 "datatype": "DateTime"
}, {
	"name": "ParentOutgoingDocEnteredByUserId",
 "datatype": "string"
}, {
	"name": "ParentOutgoingDocEnteredDateTime",
 "datatype": "DateTime"
}, {
	"name": "IsConfidential",
 "datatype": "bool"
}, {
	"name": "Attachments",
 "datatype": "string"
},]},
"name" : "IncomingDocument  
", 
{ "attributes" :[ {
	"name": "AttachedDocFileName",
 "datatype": "string"
},]},
"name" : "IncomingNotification  
", 
{ "attributes" :[ {
	"name": "NotificationType",
 "datatype": "string"
}, {
	"name": "SendDate",
 "datatype": "DateTime"
}, {
	"name": "SendByUserId",
 "datatype": "string"
}, {
	"name": "ReceivedDate",
 "datatype": "DateTime"
}, {
	"name": "Status",
 "datatype": "string"
}, {
	"name": "SenderRemarks",
 "datatype": "string"
}, {
	"name": "RecipientRemarks",
 "datatype": "string"
}, {
	"name": "isNotified",
 "datatype": "bool"
}, {
	"name": "MarkedForDeletion",
 "datatype": "bool"
},]},
"name" : "NotificationType  
", 
{ "attributes" :[ {
	"name": "Name",
 "datatype": "string"
}, {
	"name": "ForSecretary",
 "datatype": "bool"
}, {
	"name": "ForReception",
 "datatype": "bool"
}, {
	"name": "ForOthers",
 "datatype": "bool"
},]},
"name" : "OrgChartViewModel  
", 
{ "attributes" :[]},
"name" : "OrgUnit  
", 
{ "attributes" :[ {
	"name": "Name",
 "datatype": "string"
}, {
	"name": "Description",
 "datatype": "string"
}, {
	"name": "OrgUnitType",
 "datatype": "string"
}, {
	"name": "OrgUnitForm",
 "datatype": "string"
},]},
"name" : "OutgoingDistribution 
", 
{ "attributes" :[ {
	"name": "NotificationType",
 "datatype": "string"
}, {
	"name": "SendDate",
 "datatype": "DateTime"
}, {
	"name": "SendByUserId",
 "datatype": "string"
}, {
	"name": "SenderRemarks",
 "datatype": "string"
}, {
	"name": "ReceivedDate",
 "datatype": "DateTime"
}, {
	"name": "ReceivedByUserId",
 "datatype": "string"
}, {
	"name": "RecipientRemarks",
 "datatype": "string"
}, {
	"name": "Status",
 "datatype": "string"
}, {
	"name": "isNotified",
 "datatype": "bool"
}, {
	"name": "MarkedForDeletion",
 "datatype": "bool"
},]},
"name" : "OutgoingDocumentForm  
", 
{ "attributes" :[ {
	"name": "CodeLevel6Text",
 "datatype": "string"
}, {
	"name": "Recipient",
 "datatype": "string"
}, {
	"name": "SentDate",
 "datatype": "DateTime"
}, {
	"name": "ApprovedDate",
 "datatype": "DateTime"
}, {
	"name": "FinalizationDate",
 "datatype": "DateTime"
}, {
	"name": "DateEntered",
 "datatype": "DateTime"
}, {
	"name": "EnteredByUserId",
 "datatype": "string"
}, {
	"name": "Technical_GeographicalArea",
 "datatype": "string"
}, {
	"name": "Technical_Type",
 "datatype": "string"
}, {
	"name": "Technical_FromKmSpot",
 "datatype": "string"
}, {
	"name": "Technical_ToKmSpot",
 "datatype": "string"
}, {
	"name": "OnM_GeographicalArea",
 "datatype": "string"
}, {
	"name": "OnM_Type",
 "datatype": "string"
}, {
	"name": "OnM_FromKmSpot",
 "datatype": "string"
}, {
	"name": "OnM_ToKmSpot",
 "datatype": "string"
}, {
	"name": "Accounting_AFM",
 "datatype": "string"
}, {
	"name": "Accounting_ResponsibleAccountant",
 "datatype": "string"
}, {
	"name": "Accounting_PaymentDueDate",
 "datatype": "DateTime"
}, {
	"name": "Accounting_PaymentApproval",
 "datatype": "string"
}, {
	"name": "Financial_BankAccountNo",
 "datatype": "string"
}, {
	"name": "Financial_StartingDatePeriod",
 "datatype": "DateTime"
}, {
	"name": "Financial_EndingDatePeriod",
 "datatype": "DateTime"
}, {
	"name": "TollControl_AFM",
 "datatype": "string"
}, {
	"name": "TollControl_ResponsibleAccountant",
 "datatype": "string"
}, {
	"name": "TollControl_PaymentDueDate",
 "datatype": "DateTime"
}, {
	"name": "TollControl_PaymentApproval",
 "datatype": "string"
}, {
	"name": "IT_TollSystems_Task",
 "datatype": "string"
}, {
	"name": "IT_TollSystems_DateOverdue",
 "datatype": "DateTime"
}, {
	"name": "HR_Task",
 "datatype": "string"
}, {
	"name": "HR_DateOverdue",
 "datatype": "DateTime"
}, {
	"name": "Legal_Task",
 "datatype": "string"
}, {
	"name": "Legal_DateOverdue",
 "datatype": "DateTime"
}, {
	"name": "PR_Task",
 "datatype": "string"
}, {
	"name": "PR_DateOverdue",
 "datatype": "DateTime"
}, {
	"name": "QMS_Task",
 "datatype": "string"
}, {
	"name": "QMS_DateOverdue",
 "datatype": "DateTime"
}, {
	"name": "Coding",
 "datatype": "string"
}, {
	"name": "IsForDeletion",
 "datatype": "bool"
}, {
	"name": "NotificationText",
 "datatype": "string"
}, {
	"name": "ReceiptRefNo",
 "datatype": "string"
}, {
	"name": "Subject",
 "datatype": "string"
}, {
	"name": "Relevant1",
 "datatype": "string"
}, {
	"name": "Relevant2",
 "datatype": "string"
}, {
	"name": "Relevant",
 "datatype": "string"
}, {
	"name": "AttachedDocuments",
 "datatype": "string"
}, {
	"name": "IsLocked",
 "datatype": "bool"
}, {
	"name": "LastModifiedByUserId",
 "datatype": "string"
}, {
	"name": "DateLastModified",
 "datatype": "DateTime"
}, {
	"name": "LockedByUserId",
 "datatype": "string"
}, {
	"name": "DateLocked",
 "datatype": "DateTime"
}, {
	"name": "IncludeCoverLetter",
 "datatype": "bool"
}, {
	"name": "CoverLetterId",
 "datatype": "int"
}, {
	"name": "IsViolationClaim",
 "datatype": "bool"
}, {
	"name": "ParentIncomingDocEnteredByUserId",
 "datatype": "string"
}, {
	"name": "ParentIncomingDocEnteredDateTime",
 "datatype": "DateTime"
}, {
	"name": "ParentOutgoingDocEnteredByUserId",
 "datatype": "string"
}, {
	"name": "ParentOutgoingDocEnteredDateTime",
 "datatype": "DateTime"
}, {
	"name": "IsConfidential",
 "datatype": "bool"
}, {
	"name": "Attachments",
 "datatype": "string"
},]},
"name" : "OutgoingDocument  
", 
{ "attributes" :[ {
	"name": "AttachedDocFileName",
 "datatype": "string"
},]},
"name" : "ViolationStatus  
", 
{ "attributes" :[ {
	"name": "ClaimDelivered",
 "datatype": "bool"
}, {
	"name": "ClaimDeliveredDate",
 "datatype": "DateTime"
}, {
	"name": "ClaimDeliveredUser",
 "datatype": "string"
}, {
	"name": "ClaimAmountPaid",
 "datatype": "bool"
}, {
	"name": "ClaimAmountPaidDate",
 "datatype": "DateTime"
}, {
	"name": "ClaimAmountPaidUser",
 "datatype": "string"
}, {
	"name": "CourtOrderForPaymentIssued",
 "datatype": "bool"
}, {
	"name": "CourtOrderForPaymentIssuedDate",
 "datatype": "DateTime"
}, {
	"name": "CourtOrderForPaymentIssuedUser",
 "datatype": "string"
}, {
	"name": "CourtOrderAmountPaid",
 "datatype": "bool"
}, {
	"name": "CourtOrderAmountPaidDate",
 "datatype": "DateTime"
}, {
	"name": "CourtOrderAmountPaidUser",
 "datatype": "string"
}, {
	"name": "ClaimDeliveredManualDate",
 "datatype": "DateTime"
}, {
	"name": "ClaimAmountPaidManualDate",
 "datatype": "DateTime"
}, {
	"name": "CourtOrderForPaymentManualDate",
 "datatype": "DateTime"
}, {
	"name": "CourtOrderAmountPaidManualDate",
 "datatype": "DateTime"
},]},
"name" : "OutgoingNotification  
", 
{ "attributes" :[ {
	"name": "NotificationType",
 "datatype": "string"
}, {
	"name": "SendDate",
 "datatype": "DateTime"
}, {
	"name": "SendByUserId",
 "datatype": "string"
}, {
	"name": "SenderRemarks",
 "datatype": "string"
}, {
	"name": "ReceivedDate",
 "datatype": "DateTime"
}, {
	"name": "ReceivedByUserId",
 "datatype": "string"
}, {
	"name": "RecipientRemarks",
 "datatype": "string"
}, {
	"name": "Status",
 "datatype": "string"
}, {
	"name": "ReceivedByUserRole1",
 "datatype": "string"
}, {
	"name": "isNotified",
 "datatype": "bool"
}, {
	"name": "MarkedForDeletion",
 "datatype": "bool"
}, {
	"name": "OrderIndex",
 "datatype": "int"
},]},
"name" : "ProtocolNumber  
", 
{ "attributes" :[ {
	"name": "ProtocolNo",
 "datatype": "int"
}, {
	"name": "CompanyId",
 "datatype": "int"
}, {
	"name": "DocumentType",
 "datatype": "string"
}, {
	"name": "DateIssued",
 "datatype": "DateTime"
},]},
"name" : "RecipientGroup  
", 
{ "attributes" :[ {
	"name": "Name",
 "datatype": "string"
}, {
	"name": "isPublic",
 "datatype": "bool"
}, {
	"name": "DateEntered",
 "datatype": "DateTime"
}, {
	"name": "EnteredByUserId",
 "datatype": "string"
}, {
	"name": "DateModified",
 "datatype": "DateTime"
}, {
	"name": "ModifiedByUserId",
 "datatype": "string"
},]},
"name" : "RecipientGroupDetail  
", 
{ "attributes" :[ {
	"name": "OrderNo",
 "datatype": "int"
},]},
"name" : "Role  
", 
{ "attributes" :[ {
	"name": "RoleName",
 "datatype": "string"
}, {
	"name": "Description",
 "datatype": "string"
},]},
"name" : "SendersList  
", 
{ "attributes" :[ {
	"name": "FromTxt",
 "datatype": "string"
}, {
	"name": "EnteredByUserId",
 "datatype": "string"
}, {
	"name": "EnteredDate",
 "datatype": "DateTime"
}, {
	"name": "ModifiedByUserId",
 "datatype": "string"
}, {
	"name": "ModifiedDate",
 "datatype": "DateTime"
},]},
}]
'''