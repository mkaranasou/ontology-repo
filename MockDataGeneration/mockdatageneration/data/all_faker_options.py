# faker.providers.base
fake.random_digit_not_null()                                                                # 5
fake.randomize_nb_elements(number=10, le=False, ge=False)                                   # 7
fake.random_letter()                                                                        # Y

fake.random_element(array=('a', 'b', 'b'))                                                  # b
fake.bothify(text="## ??")                                                                  # 30 gU
fake.random_digit()                                                                         # 6
fake.random_number(digits=None)                                                             # 412284
fake.lexify(text="????")                                                                    # LjZK
fake.random_int(min=0, max=9999)                                                            # 7954
########################################################################################################################
# faker.providers.address
fake.numerify(text="###")                                                                   # 261
fake.latitude()                                                                             # 74.774975
fake.street_name()                                                                          # Kosmonoská
fake.address()                                                                              # Jírova 27                                                                                               442 76 Štětí
fake.street_address()                                                                       # Štěchovická 2
fake.postcode()                                                                             # 360 93
fake.longitude()                                                                            # -157.928775
fake.country()                                                                              # Ghana
fake.city_name()                                                                            # Štětí
fake.street_suffix()                                                                        # Street
fake.geo_coordinate(center=None, radius=0.001)                                              # -123.577121
fake.city_suffix()                                                                          # Ville
fake.building_number()                                                                      # 58
fake.street_suffix_long()                                                                   # náměstí
fake.street_suffix_short()                                                                  # nám.
fake.city()                                                                                 # Litovel
fake.state()                                                                                # Jihočeský kraj
########################################################################################################################
# faker.providers.color
fake.rgb_css_color()                                                                        # rgb(190,10,125)
fake.color_name()                                                                           # DarkRed
fake.rgb_color_list()                                                                       # (135, 73, 240)
fake.rgb_color()                                                                            # 127,171,216
fake.safe_hex_color()                                                                       # #dd3300
fake.safe_color_name()                                                                      # olive
fake.hex_color()                                                                            # #846c81
########################################################################################################################
# faker.providers.company
fake.company()                                                                              # Machová Vlček a.s.
fake.company_suffix()                                                                       # a.s.
########################################################################################################################
# faker.providers.credit_card
fake.credit_card_security_code(card_type=None)                                              # 854
fake.credit_card_full(card_type=None, validate=False, max_check=10)                         #                                                                                                 VISA 13 digit                                                                                                   Michael Dvořáková                                                                                               4539073594270  08/15                                                                                                    CVC: 274
fake.credit_card_expire(start="now", end="+10y", date_format="%m/%y")                       # 11/22
fake.credit_card_number(card_type=None, validate=False, max_check=10)                       # 869944946930103
fake.credit_card_provider(card_type=None)                                                   # JCB 16 digit
########################################################################################################################
# faker.providers.date_time
fake.date_time_ad()                                                                         # 0382-02-16 12:46:35
fake.month()                                                                                # 09
fake.am_pm()                                                                                # PM
fake.iso8601()                                                                              # 1988-08-26T23:35:53
fake.date_time_this_century()                                                               # 1982-04-23 09:44:01
fake.date_time()                                                                            # 1971-05-30 23:03:58
fake.month_name()                                                                           # March
fake.date_time_this_month()                                                                 # 2015-02-08 08:17:05
fake.date_time_this_decade()                                                                # 2012-06-28 14:35:41
fake.day_of_week()                                                                          # Thursday
fake.day_of_month()                                                                         # 15
fake.time(pattern="%H:%M:%S")                                                               # 01:28:23
fake.date_time_between(start_date="-30y", end_date="now")                                   # 2003-04-15 06:04:13
fake.unix_time()                                                                            # 1383462111
fake.date_time_this_year()                                                                  # 2014-03-02 03:50:48
fake.timezone()                                                                             # Indian/Antananarivo
fake.century()                                                                              # I
fake.date(pattern="%Y-%m-%d")                                                               # 2004-01-27
fake.year()                                                                                 # 2007
########################################################################################################################
# faker.providers.file
fake.mime_type(category=None)                                                               # model/iges
########################################################################################################################
# faker.providers.internet
fake.ipv4()                                                                                 # 97.173.103.30
fake.url()                                                                                  # http://www.benes.com/
fake.company_email()                                                                        # tkratochvil@stepanek.com
fake.uri()                                                                                  # http://www.hajkova.cz/list/wp-content/app/terms.htm
fake.tld()                                                                                  # cz
fake.uri_path(deep=None)                                                                    # category/category/search
fake.free_email()                                                                           # nmaskova@seznam.cz
fake.user_name()                                                                            # jarmila57
fake.free_email_domain()                                                                    # email.cz
fake.domain_name()                                                                          # kolar.cz
fake.uri_extension()                                                                        # .html
fake.ipv6()                                                                                 # cd3b:b5f3:0e91:9034:48ae:088a:9207:6b83
fake.safe_email()                                                                           # jindrich.cerny@example.org
fake.uri_page()                                                                             # author
fake.email()                                                                                # miloslava12@seznam.cz
fake.domain_word()                                                                          # jelinek
fake.slug(value=None)                                                                       # non-totam-omnis
########################################################################################################################
# faker.providers.job
fake.job()                                                                                  # Counselling psychologist
########################################################################################################################
# lorem
fake.text(max_nb_chars=200)                                                                 # Enim odit ut veniam et. Sed animi aspernatur quam porro es                                                                                                      t vitae. Alias ex veritatis aliquam eum.                                                                                                Nemo eos omnis distinctio veritatis dignissimos sunt. Odio                                                                                                       eos fugit dolor tenetur dolore iure.
fake.sentence(nb_words=6, variable_nb_words=True)                                           # Sed asperiores est est sit ipsam.
fake.word()                                                                                 # totam
fake.paragraphs(nb=3)                                                                       # [u'Est quod facere tempore consequatur provident. Aut nost                                                                                                      rum porro recusandae. Asperiores impedit voluptas consequa                                                                                                      tur quasi.', u'Provident officia accusamus quo eos. Et aut                                                                                                       amet ut. Doloremque veritatis tempora aut ea qui velit. V                                                                                                      elit mollitia quisquam et aliquid molestias sunt aut.', u'                                                                                                      Nam vitae neque ratione vel voluptatem. In id ipsa et accu                                                                                                      santium molestias. Ipsum corrupti accusamus omnis non. Mol                                                                                                      litia ab quod aut magnam eos.']
fake.words(nb=3)                                                                            # [u'consequatur', u'dignissimos', u'qui']
fake.paragraph(nb_sentences=3, variable_nb_sentences=True)                                  # Eum dolore nesciunt quidem eveniet saepe. Commodi dolores                                                                                                       tempore aut maiores nulla error ex. Illum modi dolor ullam                                                                                                       pariatur nam. Vero illo adipisci ipsam et animi.
fake.sentences(nb=3)                                                                        # [u'In dignissimos aliquam perspiciatis similique atque vol
########################################################################################################################
# misc
fake.password(length=10, special_chars=True, digits=True, upper_case=True, lower_case=True) # UM8O*#N%LF
fake.locale()                                                                               # fr_ME
fake.md5(raw_output=False)                                                                  # 554b25d542110ab17ae78e27be0d8b55
fake.sha1(raw_output=False)                                                                 # ae02022dee77be9e326bb677765842255deb6244
fake.null_boolean()                                                                         # False
fake.sha256(raw_output=False)                                                               # 682e1d93d8d184c99f9c9d7c30221d9c3e96fc385939449ea197b73495                                                                                                      828fff
fake.country_code()                                                                         # BI
fake.language_code()                                                                        # it
fake.boolean(chance_of_getting_true=50)                                                     # True
########################################################################################################################
# person
fake.last_name_male()                                                                       # Mašek
fake.prefix_male()                                                                          # Mgr.
fake.prefix()                                                                               # pan
fake.name()                                                                                 # Drahomíra Beranová
fake.first_name()                                                                           # Luděk
fake.suffix()                                                                               # Th.D.
fake.first_name_male()                                                                      # Ondřej
fake.first_name_female()                                                                    # Klára
fake.last_name_female()                                                                     # Novotná
fake.last_name()                                                                            # Novotná
fake.prefix_female()                                                                        # JUDr.
########################################################################################################################
# profile
fake.profile(fields=[])                                                                     # {'website': [u'http://bartos.cz/', u'http://kolarova.cz/',                                                                                                       u'http://dusek.cz/', u'http://stastny.cz/'], 'username':                                                                                                       u'alexander90', 'name': u'Bc. Jind\u0159i\u0161ka Proch\xe                                                                                                      1zkov\xe1', 'blood_group': 'A-', 'residence': u'\u0160tych                                                                                                      ova 068\n873 65 Man\u011bt\xedn', 'company': u'Dvo\u0159\x                                                                                                      e1k Urbanov\xe1 s.r.o.', 'address': u'Spojovac\xed 5\n269                                                                                                       79 Dob\u0159\xed\u0161', 'birthdate': '2012-06-19', 'sex':                                                                                                       'M', 'job': 'Tax inspector', 'ssn': u'692-31-0604', 'curr                                                                                                      ent_location': (Decimal('28.5080715'), Decimal('8.198786')                                                                                                      ), 'mail': u'dagmar.stastna@volny.cz'}
fake.simple_profile()                                                                       # {'username': u'maly.bretislav', 'name': u'Radovan K\u0159\
########################################################################################################################
# python
fake.pyiterable(nb_elements=10, variable_nb_elements=True, *value_types)                    # set([Decimal('259153810384'), -3495704196697.7, u'Doloremq                                                                                                      ue natus.', -460776867122617.0, -8591800.0, u'http://www.k                                                                                                      riz.cz/tag/tag/tag/login.html', 5452, 3696, datetime(1979,                                                                                                       6, 16, 7, 38, 36), u'fcerna@post.cz', u'Ut nesciunt ut.']                                                                                                      )
fake.pystr(max_chars=20)                                                                    # Laudantium ab.
fake.pyfloat(left_digits=None, right_digits=None, positive=False)                           # 7.97433652739e+12
fake.pystruct(count=10, *value_types)                                                       # ([Decimal('177014.412236'), 8657, u'http://www.nemec.cz/se                                                                                                      arch/homepage/', u'Soluta ducimus sint.', 1031, u'nemec.va                                                                                                      clav@gmail.com', -2081233032.64, 924945251431244.0, u'Opti                                                                                                      o doloremque a.', u'Porro voluptatibus.'], {u'non': Decima                                                                                                      l('-20674.0'), u'ut': 5989, u'incidunt': u'Distinctio omni                                                                                                      s.', u'harum': Decimal('-9.2667375118E+11'), u'tenetur': D                                                                                                      ecimal('41666908.7757'), u'at': Decimal('69977360.4'), u'd                                                                                                      electus': u'Molestiae maiores.', u'aliquam': u'Ex maiores                                                                                                       modi.'}, {u'quidem': {0: u'Necessitatibus.', 1: [6041, u'O                                                                                                      ccaecati doloribus.', u'Nesciunt est.'], 2: {0: 5271, 1: 4                                                                                                      73.881691, 2: [Decimal('-12755.811945'), Decimal('-84.9476                                                                                                      3172')]}}, u'et': {8: [u'Hic voluptas eos.', 3834, 7768],                                                                                                       9: {8: u'Vel a dicta.', 9: [6957, u'Iste voluptatem non.']                                                                                                      , 7: u'pavla.kratochvilova@centrum.cz'}, 7: 5607}, u'quas'                                                                                                      : {1: u'Aut voluptas quasi.', 2: [253, datetime(1981, 9, 1                                                                                                      3, 2, 53, 51), 52.12256], 3: {1: u'Tenetur perferendis.',                                                                                                       2: u'blanka.kucerova@seznam.cz', 3: [1587, 5277]}}, u'esse                                                                                                      ': {8: {8: [u'http://pokorny.com/main.php', datetime(1974,                                                                                                       10, 7, 7, 26, 29)], 6: u'Expedita sint.', 7: u'http://dol                                                                                                      ezalova.cz/app/homepage.html'}, 6: Decimal('-5.08728347544                                                                                                      E+13'), 7: [u'Quo earum neque aut.', u'Aliquam qui est.',                                                                                                       3426]}, u'quod': {5: -957907109510175.0, 6: [u'Beatae pari                                                                                                      atur.', u'In iusto qui facere.', u'Repudiandae.'], 7: {5:                                                                                                       u'Illum occaecati.', 6: 0.24779979, 7: [u'Totam dicta non.                                                                                                      ', 5248]}}, u'aspernatur': {4: u'Tenetur consequatur.', 5:                                                                                                       [9018, u'Rem voluptatum.', 6340], 6: {4: 207, 5: u'Conseq                                                                                                      uuntur qui ab.', 6: [3080, Decimal('6.66523867878E+13')]}}                                                                                                      , u'corrupti': {8: Decimal('-18890460.2965'), 9: [u'Ea asp                                                                                                      ernatur.', u'Voluptatem ea sunt.', u'Vero autem est.'], 10                                                                                                      : {8: u'simkova.vlasta@volny.cz', 9: Decimal('-1.946063026                                                                                                      38E+12'), 10: [u'jelinkova.julie@gmail.com', 72405420391.0                                                                                                      ]}}, u'tempora': {9: u'jarmila.beranova@volny.cz', 10: [u'                                                                                                      Quisquam facilis.', 290.526534, u'http://rihova.cz/terms.p                                                                                                      hp'], 11: {9: 2061, 10: Decimal('-3.30621529'), 11: [3232,                                                                                                       u'Nam molestiae nisi.']}}, u'ad': {3: u'Vitae autem et.',                                                                                                       4: [datetime(1983, 8, 24, 12, 1, 8), 807, u'http://www.va                                                                                                      nek.cz/privacy.htm'], 5: {3: u'novotny.jan@centrum.cz', 4:                                                                                                       5097, 5: [u'http://www.hajkova.cz/categories/category/',                                                                                                       u'gruzickova@post.cz']}}})
fake.pydecimal(left_digits=None, right_digits=None, positive=False)                         # -1.4102101
fake.pylist(nb_elements=10, variable_nb_elements=True, *value_types)                        # [u'Nesciunt ut ut.', u'radovan.soukup@centrum.cz', u'tbart                                                                                                      os@gmail.com', 1802, datetime(2007, 7, 31, 10, 36, 20), u'                                                                                                      jakub.vanek@email.cz', u'Sapiente.', Decimal('451160968399                                                                                                      ')]
fake.pytuple(nb_elements=10, variable_nb_elements=True, *value_types)                       # (datetime(1981, 1, 3, 17, 47, 50), u'Fugiat ea non quae.',                                                                                                       Decimal('2.07028312898E+13'), u'phajkova@seznam.cz', u'du                                                                                                      sek.bohumil@post.cz', u'In necessitatibus.', Decimal('1233                                                                                                      307.76388'), 2091914.61016182, u'Culpa officiis.', u'Paria                                                                                                      tur ipsa quod.', u'Eveniet ipsam eius.', datetime(1987, 2,                                                                                                       14, 3, 30, 13))
fake.pybool()                                                                               # True
fake.pyset(nb_elements=10, variable_nb_elements=True, *value_types)                         # set([313033250435499.0, u'Enim numquam veniam.', u'Aliquid                                                                                                       non.', u'Ipsam distinctio.', 801746107350.996, u'Eum cons                                                                                                      equatur est.', 8433846.414838, u'Minus consequatur.', 3530                                                                                                      , 9054])
fake.pydict(nb_elements=10, variable_nb_elements=True, *value_types)                        # {u'non': u'Ratione iure modi.', u'commodi': -778.8, u'sit'                                                                                                      : u'Numquam tenetur.', u'eaque': u'http://www.blazek.cz/ca                                                                                                      tegory/', u'libero': datetime(2002, 11, 15, 5, 29, 35), u'                                                                                                      molestiae': u'fstepankova@chello.cz', u'assumenda': u'http                                                                                                      ://www.novotna.com/posts/tags/posts/login/', u'rerum': Dec                                                                                                      imal('-7114190274.17'), u'voluptatem': u'Sit quod ipsum.',                                                                                                       u'nobis': u'Reprehenderit.', u'omnis': u'riha.kamil@sezna                                                                                                      m.cz'}
fake.pyint()                                                                                # 6409
########################################################################################################################
# faker.providers.phone_numbe
fake.phone_number()                                                                         # 730 862 479
########################################################################################################################
# ssn
fake.ssn()                                                                                  # 713-17-9375
########################################################################################################################
# user_agent
fake.mac_processor()                                                                        # Intel
fake.firefox()                                                                              # Mozilla/5.0 (X11; Linux x86_64; rv:1.9.7.20) Gecko/2012-04                                                                                                      -13 04:05:49 Firefox/3.6.4
fake.linux_platform_token()                                                                 # X11; Linux x86_64
fake.opera()                                                                                # Opera/8.23.(Windows NT 6.1; sl-SI) Presto/2.9.184 Version/                                                                                                      11.00
fake.windows_platform_token()                                                               # Windows NT 6.1
fake.internet_explorer()                                                                    # Mozilla/5.0 (compatible; MSIE 6.0; Windows 95; Trident/5.1                                                                                                      )
fake.user_agent()                                                                           # Opera/9.90.(Windows 98; Win 9x 4.90; en-US) Presto/2.9.160                                                                                                       Version/12.00
fake.chrome()                                                                               # Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_5_7) AppleWebKi                                                                                                      t/5330 (KHTML, like Gecko) Chrome/14.0.832.0 Safari/5330
fake.linux_processor()                                                                      # i686
fake.mac_platform_token()                                                                   # Macintosh; U; Intel Mac OS X 10_7_1
fake.safari()                                                                               # Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_8_6 rv:6.0; s
