import json
import os

__author__ = 'm.karanasou'

folder = r'C:\xampp\htdocs\MockDataGeneration\mockdatageneration\data\DomainModel\DomainModel'
output = 'domain_model.py'
results = []


class Entity(object):
    def __init__(self):
        self.name = None
        self.attributes = []

    def to_JSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                sort_keys=True, indent=4)


class EntityAttribute(object):
    def __init__(self):
        self.name = None
        self.data_type = None


def transform():
    global results
    os.chdir(folder)    # go to the dir where files are
    for filename in os.listdir(folder):
        print "Processing {0}...\n".format(filename)
        with open(filename, 'r') as temp_file:
            entity = None
            found_property = False
            line_list = temp_file.readlines()   # read file into list
            print len(line_list)
            for i in range(0, len(line_list)):
                if (line_list[i] == '\n' or line_list[i] == '\t}\n' or line_list[i] == '') and found_property:  # if in after properties section
                    found_property = False
                    if entity is not None:
                        results.append(entity)  # save entity
                    print len(entity.attributes)
                    entity = None

                if found_property:  # if in properties section
                    attribute = EntityAttribute()
                    attribute.name = line_list[i].replace("\t", "").split(" ")[1].replace(";", "")
                    attribute.data_type = line_list[i].replace("\t", "").split(" ")[0]
                    entity.attributes.append(attribute)

                if "// Properties" in line_list[i]:
                    print line_list[i]
                    entity = Entity()
                    print entity.attributes
                    entity.name = line_list[i-1].replace("\tEntity ", "").replace(" {", "").replace("\tClass ", "")
                    found_property = True
                    continue


transform()


# for each in results:
#     print('"name" : "{0}", \{{ "attributes" :['.format(each.name))
#     for attr in each.attributes:
#         print(' {{\n\t"name": "{0}",\n "datatype": "{1}"\n}},'.format(attr.name, attr.data_type))
#
#     print("]},\n")

print len(results)

with open(output, 'w') as domain_model_file:
    domain_model_file.write('domain_model = \'\'\'"classes": [{\n')
    for each in results:
        domain_model_file.write('"name" : "{0}", \n{{ "attributes" :['.format(each.name))
        for attr in each.attributes:
           domain_model_file.write(' {{\n\t"name": "{0}",\n "datatype": "{1}"\n}},'.format(attr.name, attr.data_type))

        domain_model_file.write("]},\n")
    domain_model_file.write("}]\n\'\'\'")