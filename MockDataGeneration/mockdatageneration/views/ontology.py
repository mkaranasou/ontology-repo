from flask import jsonify, request
from mockdatageneration import app, auto, InvalidUsage

__author__ = 'm.karanasou'


@app.route('/api/1.0/rdf_to_jsonld',  methods=['GET', 'POST'])
@auto.doc()
def test_rdf_parser():
    from rdflib import Graph

    g = Graph()

    if request.method == "POST":
        g.parse(data=request.data, format="application/rdf+xml")

    else:
        # g.parse(data=request.args.get('content'), format="application/rdf+xml")

        url = request.args.get('url')
        if url is not "":
            format = "xml"
            if url[-3:] == "ttl":
                format = "n3"
            g.parse(url, format=format)

    resp = app.make_response(jsonify({"ontology" : g.serialize(format='json-ld', indent=4)}))
    resp.headers['Access-Control-Allow-Origin'] = '*'

    return resp
    # else:
    #     raise InvalidUsage("Missing 'url' parameter!", 400)

    # return jsonify({"ontology": g.serialize(format='json-ld', indent=4)})
