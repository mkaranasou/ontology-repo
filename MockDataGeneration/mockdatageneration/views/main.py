# -*- coding: utf-8 -*-
import traceback
import random
from flask import Flask, jsonify, request, Response
from faker import Factory
from flask.ext.autodoc import Autodoc
from mockdatageneration.models.ExceptionHandler import ExceptionHandler
from mockdatageneration.models.RequestClass import NameHelper
from mockdatageneration.models.RequestHandler import RequestHandler

__author__ = 'm.karanasou'

app = Flask(__name__)
auto = Autodoc(app)
fake = Factory.create()


@app.route('/')
@auto.doc()
def index():
    return "Hello, World!"


@app.route('/api/1.0/tests/faker')
@auto.doc()
def get_faker_data():
    request_handler = RequestHandler(request)
    return jsonify({'profile': request_handler.process()})


@app.route('/api/1.0/categories/attributes', methods=['GET'])
@auto.doc()
def get_attribute_categories():
    """Get all the available attributes categories"""
    request_handler = RequestHandler(request)
    return jsonify(request_handler.process())


@app.route('/api/1.0/categories/class', methods=['GET'])
@auto.doc()
def get_class_categories():
    """Get all the available class categories"""
    request_handler = RequestHandler(request)
    return jsonify(request_handler.process())


@app.route('/api/1.0/categories/get_gategory_for_class/<string:class_name>', methods=['GET'])
@app.route('/api/1.0/categories/get_gategory_for_class/<path:class_name>', methods=['GET'])
@auto.doc()
def get_gategory_for_class(class_name):
    """
    Given a class_name return the category it belongs
    :param class_name:
    :return: category class belongs <string>
    """
    # todo: throws not implemented due to url parameters
    request_handler = RequestHandler(request)
    return jsonify(request_handler.process())


@app.route('/api/1.0/data/get_data', methods=['GET', 'POST'])
@auto.doc()
def generate_data():
    """Given list of classes, for each class name, find category and get random data related to attr {repeat} times."""

    request_handler = RequestHandler(request)
    results = None

    try:
        results = request_handler.process()
    except:
        print traceback.print_exc()
        raise InvalidUsage("Server Exception occurred!", status_code=500)

    if type(results) is ExceptionHandler:
        raise InvalidUsage(results.message, results.type_)

    return jsonify({results[0]: results[1]})


@app.route('/api/1.0/data/get_name', methods=['POST'])
@auto.doc()
def get_proper_name():
    """Given a class or attribute name, return the preprocessed proper name."""
    result = None
    class_name = None
    if request.method == "POST":
        try:
            if len(request.form.keys()) > 0:
                class_name = request.form["className"]
            else:
                class_name = request.data["className"]

            if len(class_name) > 1:
                name_helper = NameHelper()
                result = name_helper.get_name(class_name)
        except:
            raise
        if class_name is None:
            raise InvalidUsage("Parameter 'className' cannot be null", status_code=500)
    return jsonify({class_name: result})


@app.route('/api/1.0/data/get_data_by_category/<path:attribute><path:category><path:repeat>', methods=['GET', 'POST'])
@app.route('/api/1.0/data/get_data_by_category', methods=['GET', 'POST'])
@auto.doc()
def get_data_by_category(attribute=None, category=None, repeat=None):
    """Given an attribute name, and a category along with an integer 'repeat'
     return relative data 'repeat' times."""
    print attribute, category, repeat
    request_handler = RequestHandler(request)
    results = None

    try:
        results = request_handler.process()
    except:
        print traceback.print_exc()
        raise InvalidUsage("Server Exception occurred!", status_code=500)

    if type(results) is ExceptionHandler or results is None:
        raise InvalidUsage(results.message, results.type_)

    return jsonify({results[0]: results[1]})


@app.route('/api/1.0/test_xml', methods=['GET', 'POST'])
def test_xml():
    # if request.method == "GET":
    #     return "<test>Completed!</test>"
    # if request.method == "POST":
    #     print request.data
    #     return request.data

    return Response("<HttpAuthentication><username>username</username></HttpAuthentication>", mimetype='text/xml')



# GENERAL PURPOSE CLASSES #
class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@app.route('/api/1.0/documentation')
def documentation():
    return auto.html()


def get_faker_data_test(category, attr):
    name = ["name", "firstname", "fname", "first"]
    last_name = ["lastname", "surname", "sname", "last", "lname"]
    full_name = ["fullname"]
    age = ["age"]
    address = ["address", "street", "streetname"]

    if category == "Human":
        attr_lower = attr.lower()
        if attr_lower in name:
            return fake.first_name()
        elif "fullname" in attr_lower:
            return fake.name()
        elif attr_lower in last_name:
            return fake.last_name()
        elif attr_lower in age:
            return random.randint(10, 80)
        elif attr_lower in address:
            if "address" in attr_lower:
                return fake.address()
            elif "street" in attr_lower:
                return fake.street_name()
            return fake.address
    elif category == "Address":
        return fake.address()
    elif category == "Animal":
        pass
    elif category == "Computer":
        return fake.ipv4()
    elif category == "Company":
        return fake.company()
    elif category == "Card":
        return fake.credit_card_full(card_type=None, validate=False, max_check=10)
    elif category=="Date":
        return fake.date_time_this_century()

    return fake.word()  # lorem ipsum..


def get_auto_data(category, attr, repeat):
    result = {}
    data_list = []
    if type(attr) is list:
        data_list = attr
    else:
        data_list = attr.split(',')
    for attribute in data_list:
        temp_attr = attribute.replace(",", "")
        faker_data = []
        for each in range(0, repeat):
            faker_data.append(get_faker_data_test(category, temp_attr))
        result[temp_attr] = faker_data

    return result

