import json
from mockdatageneration.models.SuggestionHandler import SuggestionHandler
from flask import Response
from xml.etree.ElementTree import Element, SubElement, tostring
from mockdatageneration.views.decorators import crossdomain

__author__ = 'm.karanasou'

import flask
from flask import jsonify, request
from mockdatageneration import app, auto, InvalidUsage, g


@app.route('/api/1.0/mappings/suggest',  methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
@auto.doc()
def suggest_mappings():

    model = request.form["model"]
    s = ""
    if model is not None:
        suggestion_handler = SuggestionHandler(model)
        suggestion_handler.load_model()
        suggestion_handler.suggest()

        results = []
        root = Element('Suggestions')
        for each_pair in suggestion_handler.suggestions:
            # tmp = []
            child = SubElement(root, "Pair", method=each_pair[0].match.methods[0],
                                             confidence=str(round(each_pair[0].match.confidence[0])))
            left = SubElement(child, "Left", id=each_pair[0].id, name=each_pair[0].name,
                                                pseydoname=each_pair[0].pseydoname,
                                                preprocessed_name=" ".join(each_pair[0].preprocessed_name),
                                                preprocessed_pseydoname=" ".join(each_pair[0].preprocessed_pseydoname))

            right = SubElement(child, "Right", id=each_pair[1].id, name=each_pair[1].name,
                                                pseydoname=each_pair[1].pseydoname,
                                                preprocessed_name=" ".join(each_pair[1].preprocessed_name),
                                                preprocessed_pseydoname=" ".join(each_pair[1].preprocessed_pseydoname))

            # tmp.append("attribute")
            # tmp.append(each_pair[0].name)
            # tmp.append(each_pair[0].pseydoname)
            # tmp.append(" ".join(each_pair[0].preprocessed_name))
            # tmp.append(" ".join(each_pair[0].preprocessed_pseydoname))
            # tmp.append(each_pair[0].match.methods[0])
            # tmp.append(str(round(each_pair[0].match.confidence[0])))
            # tmp.append(each_pair[1].name)
            # tmp.append(each_pair[1].pseydoname)
            # tmp.append(" ".join(each_pair[1].preprocessed_name))
            # tmp.append(" ".join(each_pair[1].preprocessed_pseydoname))
            # results.append(tmp)

        for each_match in suggestion_handler.class_matches:
            for i in range(0, len(each_match.pairs)):
                tmp = []
                child = SubElement(root, "ClassMatch", method=each_match.methods[i], confidence=str(round(each_match.confidence[i], 2)))
                left = SubElement(child, "Left", id=each_match.pairs[i][0].id, name=each_match.pairs[i][0].name)
                right = SubElement(child, "Right", id=each_match.pairs[i][1].id, name=each_match.pairs[i][1].name)
                tmp.append("class")
                tmp.append(each_match.pairs[i][0].name)
                tmp.append(each_match.pairs[i][0].pseydoname)
                tmp.append(" ".join(each_match.pairs[i][0].preprocessed_name))
                tmp.append(" ".join(each_match.pairs[i][0].preprocessed_pseydoname))
                tmp.append(each_match.methods[i])
                tmp.append(str(round(each_match.confidence[i], 2)))
                tmp.append(each_match.pairs[i][1].name)
                tmp.append(each_match.pairs[i][1].pseydoname)
                tmp.append(" ".join(each_match.pairs[i][1].preprocessed_name))
                tmp.append(" ".join(each_match.pairs[i][1].preprocessed_pseydoname))
                results.append(tmp)

        # file_path = g.LOG_PATH.replace("/logs/", "/results/") + "result{0}.csv".format(g.DATESTAMP)
        # print file_path
        # with open(file_path, 'wb') as out_file:
        #     out_file.write(",".join(["type", "name", "pseydoname", "preprocessed_name", "preprocessed_pseydoname", "method", "confidence", "name", "pseydoname", "preprocessed_name", "preprocessed_pseydoname"]) + "\r\n")
        #     for line in results:
        #         print line
        #         tmp_line = ",".join(line)
        #         out_file.write(tmp_line + "\r\n")

        # for each_match in suggestion_handler.attribute_matches:
        #     for i in range(0, len(each_match.pairs)):
        #         child = SubElement(root, "AttributeMatch", method=each_match.methods[i],
        #         confidence=str(round(each_match.confidence[i], 2)))
        #         left = SubElement(child, "Left", id=each_match.pairs[i][0].id, name=each_match.pairs[i][0].name,\
        #                           preprocessed_name=" ".join(each_match.pairs[i][0].preprocessed_name),\
        #                           preprocessed_pseydoname=" ".join(each_match.pairs[i][0].preprocessed_pseydoname))
        #         right = SubElement(child, "Right", id=each_match.pairs[i][1].id, name=each_match.pairs[i][1].name,\
        #                           preprocessed_name=" ".join(each_match.pairs[i][1].preprocessed_name),\
        #                           preprocessed_pseydoname=" ".join(each_match.pairs[i][1].preprocessed_pseydoname))

        s = tostring(root)

    resp = flask.Response(s)
    # resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

