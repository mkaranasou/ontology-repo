from neomodel import StructuredNode, StringProperty, IntegerProperty, RelationshipTo, RelationshipFrom

__author__ = 'm.karanasou'


# class Country(StructuredNode):
#     code = StringProperty(unique_index=True, required=True)
#
#     # traverse incoming IS_FROM relation, inflate to Person objects
#     inhabitant = RelationshipFrom('Person', 'IS_FROM')
#
#
# class Person(StructuredNode):
#     name = StringProperty(unique_index=True)
#     age = IntegerProperty(index=True, default=0)
#
#     # traverse outgoing IS_FROM relations, inflate to Country objects
#     country = RelationshipTo(Country, 'IS_FROM')


class MClass(StructuredNode):
    name = StringProperty()
    # is_a_mclass = RelationshipFrom('MClass', 'IS_A')
    has_a_mclass = RelationshipTo('MClass', 'HAS_A')
    has_a_mattr= RelationshipTo('MAttribute', 'HAS_A')


class MAttribute(StructuredNode):
    name = StringProperty()
    data_type = StringProperty()
    is_part_of = RelationshipFrom('MClass', 'PART_OF')