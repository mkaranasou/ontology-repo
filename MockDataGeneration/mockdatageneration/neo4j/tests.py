# -*- coding: iso-8859-15 -*-
# from nodes import MClass, MAttribute
import ast
import random
import pymongo
from mockdatageneration.data.domain_model import domain_model
from mockdatageneration.database import MongoDBConnector
from py2neo import neo4j
from mockdatageneration.helpers.globals import g
from py2neo import Graph
from py2neo.ext.ogm import Store
from py2neo.packages.httpstream import http
# http://stackoverflow.com/questions/27078352/py2neo-2-0-errorhttpstream-socketerror-timed-out
http.socket_timeout = 9999
from py2neo import Node, Relationship
import timeit

already_seen_nodes = []
__author__ = 'm.karanasou'
graph = neo4j.Graph("http://neo4j:123456@localhost:7474/db/data/")

'''
export NEO4J_REST_URL=http://user:password@localhost:7474/db/data/
or
SET NEO4J_REST_URL=http://user:password@localhost:7474/db/data/
'''

class EnglishConcept(object):

    def __init__(self, id):
        self.id = id

    def __str__(self):
        return self.id



# def create_protocol_graph():
#     for cls in domain_model["classes"]:
#         m_class = MClass(name=cls["name"].encode('utf-8'))
#         m_class.save()
#
#         for each in cls["attributes"]:
#
#             m_attr = MAttribute(name=each["name"].encode('utf-8'), data_type=each["datatype"].encode('utf-8'))
#             m_attr.save()
#             m_class.has_a_mattr.connect(m_attr)

def py2neo_example():
    graph = Graph("http://neo4j:123456@localhost:7474/db/data/")

    for cls in domain_model["classes"]:
        m_class = Node("Class", name=cls["name"].encode('utf-8'))

        for each in cls["attributes"]:

            attr = Node("Attribute", name=each["name"].encode('utf-8'), data_type=each["datatype"].encode('utf-8'))
            class_has_a = Relationship(m_class, "HAS_A", attr)
            graph.create(class_has_a)

# py2neo_example()


def tweets_py2neo_example():
    graph = Graph("http://neo4j:neo4j1@@localhost:7474/db/data/")
    q1 = "select id, text, initial_score, feature_dict " \
         "from SentiFeed.TweetTestData;"
    # q2 = "select id, text, initial_score, feature_dict, pos_tagged_text, words, pnn " \
    #      "from SentiFeed.TweetFinalTestData;"
    feature_nodes = []
    tweets_test = g.mysql_conn.execute_query(q1)
    # tweets_trial= g.mysql_conn.execute(q2)

    positive = Node("Class", value=[3, 4, 5], name="positive")
    somewhat_positive = Node("Class", value=[1, 2], name="somewhat_positive")
    neutral = Node("Class", value=[0], name="neutral")
    somewhat_negative = Node("Class", value=[-1, -2], name="somewhat_negative")
    negative = Node("Class", value=[-3, -4, -5], name="negative")

    for each in selected_features:
        feature_nodes.append(Node("Feature", name=each))


    for row in tweets_test:
        feature_dict = ast.literal_eval(row[3])

        tweet_node = Node("Tweet", id=row[0], text=row[1], initial_score=row[2])

        score = int(round(row[2]))

        if score in range(3, 6):
            class_node = positive
        elif score in range(1, 3):
            class_node = somewhat_positive
        elif score in range(-6, -3):
            class_node = negative
        elif score in range(-3, -1):
            class_node = somewhat_negative
        else:
            class_node = neutral

        class_is = Relationship(tweet_node, "IS", class_node)
        graph.create(class_is)
        for feature, value in feature_dict.items():
            if feature in selected_features and (value =="True" or value=="False"):
                feat = feature_nodes[selected_features.index(feature)]
            else:
                feat = Node("Feature", name=feature.encode('utf-8'))
            class_has_a = Relationship(tweet_node, value, feat)
            graph.create(class_has_a)

def update_labels():
    graph = neo4j.Graph("http://neo4j:!291119851$Mk@localhost:7474/db/data/")
    store = Store(graph)
    i=0
    results = graph.cypher.execute("start n=node(*) where n.id=~'/c/en/.*' and LENGTH(LABELS(n)) = 0 "
                                   "RETURN n LIMIT 100000; ")
    for each in results:
        i += 1
        if "id" in each[0].properties:
            node = each[0]
            node.labels.add("EnglishConcept")
            node.push()

            if i % 1000 == 0:
                print i, node.properties["id"].encode('utf-8')
            # print node.properties["id"].encode('utf-8')

def mongo_db():
    data = []
    client = pymongo.MongoClient("127.0.0.1", 27017)
    total_english_concepts = 9239412
    taken = 0
    for i in range(0, total_english_concepts, 100):
        cursor = client.ConceptNet["conceptnet5.3"].find({"start": {"$regex": "/c/en/.*"}}, None).skip(i).limit(100)
        print cursor.count(with_limit_and_skip=True)
        # data = list(cursor)
        data = ((each["start"], each["rel"], each["end"]) for each in cursor)


def mongo_db_insert_all_nodes():
    data = []
    final_data = []
    client = pymongo.MongoClient("127.0.0.1", 27017)
    # total_english_concepts = 9239412
    # step = 1000000
    finalset = set()
    for i in range(0, 100000, 100):
        data = []
        cursor = client.ConceptNet["conceptnet5.3"].find({"start": {"$regex": "/c/en/.*"}}, None).skip(i).limit(100)
        for each in cursor:
            data.append(each["start"])
            data.append(each["end"])
            # final_data.append(each)
        nodes = set([x for x in data])
        finalset = finalset.union(nodes)
        # finalset.update(nodes)
        print i

    print len(finalset)
    # for i in finalset:
    #         if "murder" in i:
    #                 print i

    for node in sorted(finalset):
        save_node_in_ne4j(node)

    # for node in final_data:
    #     save_relations(node)
    #

def save_node_in_ne4j(node):
    global already_seen_nodes
    pos = None
    extra_rel = None
    lang = "en" if "/c/en/" in node else None
    node = node.replace("/c/en/", "")
    pos_part = node[-2:]

    if "/n" in pos_part or "/n/" in node:
        pos = "NN"
    if "/a" in pos_part or "/a/" in node:
        pos = "ADJ"
    if "/r" in pos_part or "/r/" in node:
        pos = "ADV"
    if "/v" in pos_part or "/v/" in node:
        pos = "VB"

    if pos is not None:
        split_node = node.split("/")
        node = split_node[0]
        if len(split_node) > 2:
            extra_rel = split_node[2]

    existing_node = None
    # exists = already_seen_nodes # graph.find("Concept", "concept", node)
    neo_node = Node("Concept", concept=node, pos=pos, lang=lang)
    if node in already_seen_nodes:
        existing_node = graph.cypher.execute('MATCH (n) WHERE n.concept="{0}" and n.pos="{1}" RETURN n LIMIT 1;'.format(node, pos))
    # existing_node = next(exists, None)
    existing_node_has_records = existing_node is not None and len(existing_node.records) > 0
    if extra_rel is None:
        if not existing_node_has_records:  # if node is like /c/en/someword and we have not stored it before
            tmp_node = graph.create(neo_node)
            already_seen_nodes.append(tmp_node[0])
        if existing_node_has_records:
            if existing_node.one.properties["pos"] != pos:
                graph.create(neo_node)
            else:
               g.logger.info("{0}".format(existing_node.one.properties["id"]))
    else:
        neo_node_context = Node("Context", concept=extra_rel)
        if not existing_node_has_records:  # if node is like /c/en/someword and we have not stored it before
            rel = Relationship(neo_node, "IsA", neo_node_context)
            graph.create(rel)
            already_seen_nodes.append(node)
        if existing_node_has_records:
            if existing_node.one.properties["pos"] != pos:
                rel = Relationship(neo_node, "IsA", neo_node_context)
                graph.create(rel)
            else:
                rel = Relationship(existing_node.one, "IsA", neo_node_context)
                graph.create(rel)


def get_relations_data():
    final_data = []
    client = pymongo.MongoClient("127.0.0.1", 27017)
    total_english_concepts = 1000000
    step = 1000
    for i in range(0, total_english_concepts, step):
        cursor = client.ConceptNet["conceptnet5.3"].find({"start": {"$regex": "/c/en/.*"}}, None).skip(i).limit(step)
        if cursor.count(with_limit_and_skip=True) == 0:
            break

        for each in cursor:
            final_data.append(each)
        print i
        if i % 100 == 0:
            for node in final_data:
                # print node["rel"]
                save_relations(node)
            final_data = []

def save_relations(node):
    # for node in data:
    specific_lang = None
    relation = node["rel"].replace("/r/", "")

    if "/" in relation:
        tmp = relation.split("/")
        specific_lang = tmp[0]
        relation = tmp[1]

    split_node_start = get_normalized_node(node["start"].replace("/c/en/", ""))
    split_node_end = get_normalized_node(node["end"].replace("/c/en/", ""))
    start = "MATCH (n) where n.concept='" + split_node_start + "' RETURN n LIMIT 1; "
    end = "MATCH (n) where n.concept='" + split_node_end + "' RETURN n LIMIT 1; "
    db_node_start = graph.cypher.execute(start)
    db_node_end = graph.cypher.execute(end)
    if db_node_start.one.properties["concept"].decode('utf-8') != "" and db_node_end.one.properties["concept"].decode('utf-8') != "":
        print db_node_start.one, db_node_end.one
        rel = Relationship(db_node_start.one, relation, db_node_end.one, {
                                                                "id": node["id"],
                                                                "weight": node["weight"],
                                                                "license": node["license"],
                                                                "context": node["context"],
                                                                "dataset": node["dataset"],
                                                                "source_uri": node["source_uri"],
                                                                "specific_lang": specific_lang
                                                                })
        print "found one"

        # graph.create(rel)
    else:
        g.logger.error("Could not locate\t{0}".format(node["id"]).decode('utf-8'))

def get_normalized_node(node):
    pos = None
    pos_part = node[-2:]
    if "/n" in pos_part or "/n/" in node:
        pos = "NN"
    if "/a" in pos_part or "/a/" in node:
        pos = "ADJ"
    if "/r" in pos_part or "/r/" in node:
        pos = "ADV"
    if "/v" in pos_part or "/v/" in node:
        pos = "VB"

    if pos is not None:
        split_node = node.split("/")
        node = split_node[0]

    return node


    # graph.create(rel)

def test_already_seen():
    global already_seen_nodes
    for i in range(0, 10):
        neo_node = Node("Concept", concept="murder", pos="nn", lang="en")
        if neo_node in already_seen_nodes:
            print "already seen"
        else:
            print "not seen "
            rel = Relationship(neo_node, "IsA", neo_node)
            neo_node_tmp = graph.create(rel)
            already_seen_nodes.append(neo_node_tmp[0].nodes[0])

# test_already_seen()
mongo_db_insert_all_nodes()
# get_relations_data()
