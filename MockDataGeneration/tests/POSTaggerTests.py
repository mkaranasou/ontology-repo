import unittest
from mockdatageneration.models.POSTag import POSTagger

__author__ = 'm.karanasou'


class TextSimilarityTests(unittest.TestCase):
    def check_basic_models(self):
        pos_tagger = POSTagger()
        self.assertEqual(pos_tagger.pos_tag(["Alarm", "Message"]), "Alarm")
        self.assertEqual(pos_tagger.pos_tag(["Customs", "Declaration"]), "Customs")
        self.assertEqual(pos_tagger.pos_tag(["Flight", "Info"]), "Info")
        self.assertEqual(pos_tagger.pos_tag(["Loss", "Event"]), "Loss")
        self.assertEqual(pos_tagger.pos_tag(["Message", "Interchange"]))
        self.assertEqual(pos_tagger.pos_tag(["Parcel", "Status", "Change"]), "Status")
        self.assertEqual(pos_tagger.pos_tag(["Postal", "Operator"]), "Operator")
        self.assertEqual(pos_tagger.pos_tag(["Priority", "Risk"], "Risk"))
        self.assertEqual(pos_tagger.pos_tag(["Alarm", "Type"]), "Alarm")
        self.assertEqual(pos_tagger.pos_tag(["Parcel"]), "Parcel")
        self.assertEqual(pos_tagger.pos_tag(["Customs", "Delcaration", "CN23"]), "Customs")
        self.assertEqual(pos_tagger.pos_tag(["Document", "Type"]), "Document")