import unittest

__author__ = 'm.karanasou'


class General(unittest.TestCase):

    def convert_types_to_dict(self):
        file_name = '../data/csharp_reserved_kw.txt'
        result = ''
        file = open(file_name, "r")

        for line in file.readlines():
            stripped_line = line.strip("\n")
            temp = "{{\"{0}\", \"{1}\"}},\n".format(stripped_line, "@"+stripped_line)
            result+= temp

        file.close()
        print result

if __name__ == '__main__':
    unittest.main()