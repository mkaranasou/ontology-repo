__author__ = 'm.karanasou'
from nltk.corpus import wordnet as wn

def syn(word, lch_threshold=2.26):
    for net1 in wn.synsets(word):
        for net2 in wn.all_synsets():
            try:
                lch = net1.lch_similarity(net2)
            except:
                continue
            # The value to compare the LCH to was found empirically.
            # (The value is very application dependent. Experiment!)
            if lch >= lch_threshold:
                yield (net1, net2, lch)

# for x in syn('love'):
#     print x
#
# for ss in wn.synsets('name'): # Each synset represents a diff concept.
#   # print ss.definition
#   print ss.lemma_names

# print wn.synsets('name')

# syns = wn.synsets('car')
# print  [s.lemmas[0].name for s in syns]