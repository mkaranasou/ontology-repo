import unittest

from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic

from mockdatageneration.helpers.globals import g

brown_ic = wordnet_ic.ic('ic-brown.dat')
semcor_ic = wordnet_ic.ic('ic-semcor.dat')

__author__ = 'm.karanasou'



class TextSimilarityTests(unittest.TestCase):
    def test_mock_data_generation(self):
            # http://stackoverflow.com/questions/20075335/is-wordnet-path-similarity-commutative
            # is it spelled correctly?
            # 1. try as is

            # 2. try synonyms

            # 3. try semantic similarity
            class_names = ["Person", "Customer", "Latitude", "Dog", "Country", "Car", "Message"]
            class_attributes = [["Name", "Surname"], ["FirstName"], ["Latitude"], ["Name"], ["Name"], ["Make"], ["Text"]]
            categories = ["Human", "Country", "Object", "Car", "Region"]

            similarity_functions = [wn.path_similarity, wn.wup_similarity, wn.res_similarity, wn.lch_similarity, wn.lin_similarity ]

            g.logger.info("Test for class names: {0}".format(class_names))
            g.logger.info("With categories: {0}".format(categories))
            for func in similarity_functions:
                g.logger.info("using : {0}".format(func.__str__()))
                results =[]

                for cls in class_names:
                    cls_res = []
                    semantic_sim_per_category = []

                    for each in categories:
                        a = wn.synset('{0}.n.01'.format(cls.lower()))
                        b = wn.synset('{0}.n.01'.format(each.lower()))

                        sim = func(a, b, brown_ic)
                        # sim = semantic_analyser.compare(cls, each)
                        # sim = SemanticAnalyzer().example(cls, each, "n", "lin")
                        semantic_sim_per_category.append([cls+" is " + each, sim])
                        cls_res.append(sim)
                    max_ = max(cls_res)
                    results.append(categories[cls_res.index(max_)])  # saves category name
                    g.logger.info("{0} is {1} with score {2}".format(cls, categories[cls_res.index(max_)], cls_res[cls_res.index(max_)]))
                g.logger.info(semantic_sim_per_category)
                names = g.mysql_conn.execute_query("SELECT first_name FROM znextdatageneration.gd_first_names; ")
                countries = g.mysql_conn.execute_query("SELECT country FROM znextdatageneration.gd_countries; ")
                objects = ["mpla mpla", "mpla", "mpla mpla", "mpla", "mpla mpla", "mpla", "mpla mpla", "mpla"]
                cars = ["Mazda", "Audi", "VW", "BMW", "Lamborghini", "Datsun", "Opel", "MG"]
                region = ["0.111", "0.928", "0.52", "3.25", "50.23", "80.3", "90.00", "10.2645"]

                data_sets = [names, countries, objects, cars, region]

                for cls in class_names:
                    res = ""
                    cls_index = class_names.index(cls)
                    res += "\n'" + cls + "':{\n"
                    for attr in class_attributes[cls_index]:
                        res += "'{0}':'{1}',\n".format(attr, data_sets[categories.index(results[cls_index])][cls_index])
                    res += "\n}"
                    g.logger.info(res)
                g.logger.info(g.log_separator)

            # cls = Classifier(g.CLASSIFIER_TYPE.NBayes, cls_helper_type=g.CLASSIFIER_TYPE.DecisionTree)
            # cls.set_x_train([])
            # cls.set_y_train([])
            # cls.train()
            # cls.set_x_trial([])
            #
            # predictions = cls.predict()

    def test_fake_factory(self):
        # https://pypi.python.org/pypi/fake-factory
        from faker import Factory
        fake = Factory.create()
        print fake.name()
        print fake.address()
        print fake.profile()
        print fake.ssn()


if __name__ == '__main__':
    unittest.main()