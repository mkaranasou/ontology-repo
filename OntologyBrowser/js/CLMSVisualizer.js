/**
 * Created by m.karanasou on 6/16/2015.
 */

/*** Layout Formats ***/

/**
 *
 * @param options
 */

var GraphNode = function(parent){
    var self = this;
    this.id =  ko.observable('');
    this.name =  ko.observable('');
    this.shortName =  ko.observable('');
    this.type = ko.observable('');
    this.size = 0;
    this.children = ko.observableArray([]);
    this.subclassOf =  ko.observableArray([]);
    this.disjoinedWith =  ko.observableArray([]);
    this.attributes = ko.observableArray([]);
    this.isType = false;
    this.parent =  parent;
};

GraphNode.prototype.addParentClass = function(parent){
    this.subclassOf.push(parent);
};

var findNodeById = function(nodes, id){
    return ko.utils.arrayFirst(nodes(), function(node){
        return node.id() === id;
    })
};

/*var VTree = function(){
    this.nodes = ko.observableArray([]);
    this.links = ko.observableArray([]);
    this.literals = ko.observableArray([]);
    var self = this;

    this.init = function(){
        this.nodes = ko.observableArray([]);
        this.links = ko.observableArray([]);
        this.literals = ko.observableArray([]);
    };

    this.load = function(options) {
        this.options = options || {};
        var self = this;
        self.init();

        // for each node in array
        $.each(options, function (k, node) {
            // create a new Graph node
            var graphNode = findNodeById(self.nodes, node["@id"]);
            var index = self.nodes.indexOf(graphNode);
            var isNew = false;
            if (graphNode == null) {
                graphNode = new GraphNode(self);
                isNew = true;
            }

            graphNode.id(node["@id"]);
            graphNode.name(node["@id"]);
            graphNode.shortName(node["@id"].split('#')[1]);
            graphNode.type(node["@type"]);

            for (var property in node) {
                if (node.hasOwnProperty(property)) {
                    if (property.toString().indexOf("#subClassOf") > -1 || property.toString().indexOf("#disjointWith")>-1) {
                        for (var i =0; i < node[property].length; i++) {

                            var parentClass = findNodeById(self.nodes, node[property][i]["@id"]);
                            if (parentClass != null) {
                                graphNode.addParentClass(parentClass)
                            }
                            else {
                                parentClass = new GraphNode();
                                parentClass.id(node[property][i]["@id"]);
                                parentClass.name(node[property][i]["@id"]);
                                graphNode.addParentClass(parentClass)
                            }
                        }
                    }
                    else if (property.toString().indexOf("#") > -1) {  // it has attributes
                        graphNode.attributes.push(node[property]);
                    }
                }
            }

            if (isNew) {
                self.nodes.push(graphNode);
            }
            else {
                self.nodes[index] = graphNode;
            }
        });

        this.getLinks();
        this.showTree();
    };

    this.getLinks = function(){
        ko.utils.arrayForEach(self.nodes(), function(node){

           if(node.subclassOf().length >0){
               var index = self.nodes().indexOf(node);
               ko.utils.arrayForEach(node.subclassOf(), function(subclass){
                   var n = findNodeById(self.nodes, subclass.id());
                     var subClassIndex = self.nodes().indexOf(n);
                   if(subClassIndex>-1){
                        self.links.push({"source":subClassIndex, "target": index, "weight": 1});
                   }
               });
           }
        });
    };

    this.showTree = function(){
       //new D3Helper().loadForce(this.nodes(), self.links(), "#ontologyRepresentation");
    }
};

var Force = function(){
    this.nodes = ko.observableArray([]);
    this.typeNodes = ko.observableArray([]);
    this.links = ko.observableArray([]);
    this.literals = ko.observableArray([]);
    var self = this;

    this.init = function(){
        this.nodes = ko.observableArray([]);
        this.links = ko.observableArray([]);
        this.literals = ko.observableArray([]);
    };

    this.load = function(options) {
        this.options = options || {};
        var self = this;
        self.init();

        // for each node in array
        $.each(options, function (k, node) {
            // create a new Graph node
            var graphNode = findNodeById(self.nodes, node["@id"]);
            var index = self.nodes.indexOf(graphNode);
            var isNew = false;
            if (graphNode == null) {
                graphNode = new GraphNode(self);
                isNew = true;
            }

            graphNode.id(node["@id"]);
            graphNode.name(node["@id"]);
            graphNode.shortName(node["@id"].split('#')[1]);
            graphNode.type(node["@type"]);

            if(node["@type"]!=undefined) {

                ko.utils.arrayForEach(graphNode.type(), function (nodeType) {
                    if (self.typeNodes().indexOf(nodeType) == -1) {
                        self.typeNodes.push(nodeType);
                    }
                });
            }

            for (var property in node) {
                if (node.hasOwnProperty(property)) {
                    if (property.toString().indexOf("#subClassOf") > -1 || property.toString().indexOf("#disjointWith")>-1) {
                        for (var i =0; i < node[property].length; i++) {

                            var parentClass = findNodeById(self.nodes, node[property][i]["@id"]);
                            if (parentClass != null) {
                                graphNode.addParentClass(parentClass)
                            }
                            else {
                                parentClass = new GraphNode();
                                parentClass.id(node[property][i]["@id"]);
                                parentClass.name(node[property][i]["@id"]);
                                parentClass.children.push(graphNode);
                                graphNode.addParentClass(parentClass)
                            }
                        }
                    }
                    else if (property.toString().indexOf("#") > -1) {  // it has attributes
                        graphNode.attributes.push(node[property]);
                    }
                }
            }

            if (isNew) {
                self.nodes.push(graphNode);
            }
            else {
                self.nodes[index] = graphNode;
            }
        });

        this.addTypeNodes();
        this.getLinks();
        this.showForce();
    };

    this.addTypeNodes = function(){
            console.log("TYPENODES :", self.typeNodes());
            ko.utils.arrayForEach(self.typeNodes(), function (nodeType) {
                    var graphNode = new GraphNode(self);
                    graphNode.id(nodeType);
                    graphNode.name(nodeType);
                    self.nodes.push(graphNode);
            });
    };

    this.getLinks = function(){

        ko.utils.arrayForEach(self.nodes(), function(node){
            var index = self.nodes().indexOf(node);
            if(node.subclassOf().length >0) {

                ko.utils.arrayForEach(node.subclassOf(), function (parentClass) {
                    var n = findNodeById(self.nodes, parentClass.id());
                    var parentClassIndex = self.nodes().indexOf(n);
                    if (parentClassIndex > -1) {
                        self.links.push({"source": parentClassIndex, "target": index, "weight": 1});
                    }
                     else{
                        console.log("could not find parent class :", parentClass.id() );
                    }
                });
            }

            if(node.type()!=undefined){
                ko.utils.arrayForEach(node.type(), function (nodeType) {
                    var n = findNodeById(self.nodes, nodeType);
                    var typeIndex = self.nodes().indexOf(n);

                    if (typeIndex > -1) {
                        self.links.push({"source": index, "target": typeIndex, "weight": 1});
                    }
                    else {
                        console.log("could not find ", nodeType);
                    }
                });
            }
        });
    };

    this.showForce = function(){
        console.log(this.nodes);
       new D3Helper().loadForce(this.nodes(), self.links(), "#ontologyRepresentation");
    }

};

var HTree = function(){
    this.load = function(options){
        var options = options || {};
    };
};*/

var CLMSVisualizer = function(){
    this.nodes = ko.observableArray([]);
    this.typeNodes = ko.observableArray([]);
    this.links = ko.observableArray([]);
    this.literals = ko.observableArray([]);
    this.d3Helper = ko.observable(new D3Helper());
    var self = this;

    this.init = function(){
        this.nodes = ko.observableArray([]);
        this.links = ko.observableArray([]);
        this.literals = ko.observableArray([]);
    };

    this.load = function(options) {
        this.options = options || {};
        var self = this;
        self.init();

        if(options != undefined) {

            // for each node in array
            $.each(options, function (k, node) {
                // create a new Graph node
                var graphNode = findNodeById(self.nodes, node["@id"]);
                var index = self.nodes.indexOf(graphNode);

                if (graphNode == null) {
                    graphNode = new GraphNode(self);
                    isNew = true;
                }

                graphNode.id(node["@id"]);
                graphNode.name(node["@id"]);
                graphNode.shortName(node["@id"].split('#')[1]);
                graphNode.type(node["@type"]);

                if (node["@type"] != undefined) {

                    ko.utils.arrayForEach(graphNode.type(), function (nodeType) {
                        if (self.typeNodes().indexOf(nodeType) == -1) {
                            self.typeNodes.push(nodeType);
                        }
                    });
                }

                for (var property in node) {
                    if (node.hasOwnProperty(property)) {
                        if (property.toString().indexOf("#subClassOf") > -1 || property.toString().indexOf("#disjointWith") > -1) {
                            for (var i = 0; i < node[property].length; i++) {

                                var parentClass = findNodeById(self.nodes, node[property][i]["@id"]);
                                if (parentClass != null) {
                                    graphNode.addParentClass(parentClass)
                                }
                                else {
                                    parentClass = new GraphNode();
                                    parentClass.id(node[property][i]["@id"]);
                                    parentClass.name(node[property][i]["@id"]);
                                    graphNode.shortName(node["@id"].split('#')[1]);
                                    parentClass.children.push(graphNode);
                                    graphNode.addParentClass(parentClass)
                                }
                            }
                        }
                        else {  // it has attributes if (property.toString().indexOf("#") > -1)

                            if(property!="@id" && property!="@type"){
                                if (property.indexOf("#")>0){
                                    graphNode.attributes.push({ attribute: property.split("#")[1], value:node[property]});
                                }
                                else{
                                    graphNode.attributes.push({attribute: property, value : node[property]});
                                }
                            }
                        }
                    }
                }

                if (index == -1) {
                    self.nodes.push(graphNode);
                }
                else {
                    self.nodes[index] = graphNode;
                }
            });

            this.addTypeNodes();
            this.getLinks();
            this.showClassHierarchy();
            this.showForce();
        }
    };

    this.addTypeNodes = function(){
            ko.utils.arrayForEach(self.typeNodes(), function (nodeType) {

                    var graphNode = findNodeById(self.nodes, nodeType);
                    var index = self.nodes.indexOf(graphNode);
                    if (graphNode == undefined) graphNode = new GraphNode(self);

                    graphNode.id(nodeType);
                    graphNode.name(nodeType);
                    if (nodeType.indexOf("#")>0){
                        graphNode.shortName(nodeType.split('#')[1]);
                    }
                else{
                        graphNode.shortName(nodeType);
                    }
                    graphNode.isType = true;

                    if (index == -1) {
                        self.nodes.push(graphNode);
                    }
                    else {
                        self.nodes[index] = graphNode;
                    }
            });
    };

    this.getLinks = function(){

        ko.utils.arrayForEach(self.nodes(), function(node){
            var index = self.nodes().indexOf(node);
            if(node.subclassOf().length >0) {

                ko.utils.arrayForEach(node.subclassOf(), function (parentClass) {
                    var n = findNodeById(self.nodes, parentClass.id());
                    var parentClassIndex = self.nodes().indexOf(n);
                    if (parentClassIndex > -1) {
                        self.links.push({"source": parentClassIndex, "target": index, "weight": 1});
                    }
                     else{
                        console.log("could not find parent class :", parentClass.id() );
                    }
                });
            }

            if(node.type()!=undefined){
                ko.utils.arrayForEach(node.type(), function (nodeType) {
                    var n = findNodeById(self.nodes, nodeType);
                    var typeIndex = self.nodes().indexOf(n);

                    if (typeIndex > -1) {
                        self.links.push({"source": index, "target": typeIndex, "weight": 1});
                    }
                    else {
                        console.log("could not find ", nodeType);
                    }
                });
            }
        });
    };

    this.showForce = function(){
       self.d3Helper().loadForce(this.nodes(), self.links(), "#ontologyRepresentation");
    };

    this.showClassHierarchy = function(){
        self.d3Helper().loadClassHierarchy(this.nodes(), self.links(), "#ontologyTree");
    }

};

