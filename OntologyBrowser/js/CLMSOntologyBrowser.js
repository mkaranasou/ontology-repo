/**
 * Created by m.karanasou on 6/15/2015.
 */


var JSONLd;

var Ontology = function(options){
    this.options = options || {};
    this.visualizer = new CLMSVisualizer();
    /*this.force = new Force();
    this.hTree = new HTree();
    this.vTree = new VTree();
*/

    this.load = function(jsonld) {

        this.json_ld = JSON.parse(jsonld.ontology);
        this.visualizer.load(this.json_ld);
        /*this.force.load(this.json_ld);
        this.hTree.load(this.json_ld);
        this.vTree.load(this.json_ld);*/
    };
};

var CLMSOntologyBrowser = function(){
    this.ontology = new Ontology();
};

CLMSOntologyBrowser.prototype.init = function(){

};

CLMSOntologyBrowser.prototype.load = function(){


};

var parseRDF = function(url){
    var url_to_analyse = url || encodeURIComponent($("#input_url").val());
    console.log(url_to_analyse);
    jQuery.ajax({
    url: "http://192.168.2.28:8082/api/1.0/rdf_to_jsonld?url="+url_to_analyse,
        type : "GET",
        dataType: "json",
        crossDomain: true,
        data: {content: ""}
    }).done(function(data) {
        JSONLd = data.ontology;
        ontologyBrowser.ontology.load(data);
    });
};

function showHideOptions(){
    $("#options").toggleClass("menu-visible");
  //
  //var options = $("#options");
  //
  //  if(options.hasClass(""))
}