/**
 * Created by m.karanasou on 6/18/2015.
 */

var D3Helper = function () {
    this.svg = null;
    /* Set the diagrams Height & Width */
    this.h = 1000;
    this.w = 1000;
    /* Set the color scale we want to use */
    this.color = d3.scale.category20c();
    var self = this;
    this.vis;

    this.loadForce = function (nodes, links, selector) {
        /* Establish/instantiate an SVG container object */
        $(selector).html("");

        this.h = parseInt($(".ontologyContainer").css('height').toString().replace("px", ""));
        this.w = parseInt($(".ontologyContainer").css('width').replace("px", ""));
        this.svg = d3.select(selector)
            .append("svg")
            .attr("height", this.h)
            .attr("width", this.w);
        /* Build the directional arrows for the links/edges */
        this.svg.append("svg:defs")
            .selectAll("marker")
            .data(["end"])
            .enter().append("svg:marker")
            .attr("id", String)
            .attr("viewBox", "0 -5 10 10")
            .attr("refX", 15)
            .attr("refY", -1.5)
            .attr("markerWidth", 6)
            .attr("markerHeight", 6)
            .attr("orient", "auto")
            .append("svg:path")
            .attr("d", "M0,-5L10,0L0,5");


        this.vis = this.svg
            .append('svg:g')
            .call(d3.behavior.zoom().on("zoom", this.redraw))
            .append('svg:g');

        this.vis.append('svg:rect')
            .attr('height', this.h)
            .attr('width', this.w)
            .attr('fill', 'white');

        this.makeForceDiag(nodes, links);
    };

    this.loadClassHierarchy = function (nodes, links, selector) {

        var margin = {top: 10, right: 20, bottom: 30, left: 20},
            width = 960 - margin.left - margin.right,
            barHeight = 20,
            barWidth = width * .2;

        var i = 0;
        var duration = 400;
        var root;
        var self = this;

        var tree = d3.layout.tree()
            .nodeSize([0, 20]);

        var diagonal = d3.svg.diagonal()
            .projection(function (d) {
                return [d.y, d.x];
            });

        this.update = function(source) {
            // Compute the flattened node list. TODO use d3.layout.hierarchy.
            var nodes = tree.nodes(root);

            var height = Math.max(500, nodes.length * barHeight + margin.top + margin.bottom);

            d3.select("svg").transition()
                .duration(duration)
                .attr("height", height);

            d3.select(self.frameElement).transition()
                .duration(duration)
                .style("height", height / 2 + "px")
                .style("overflow", "hidden");

            // Compute the "layout".
            nodes.forEach(function (n, i) {
                n.x = i * barHeight;
            });

            // Update the nodes�
            var node = svg.selectAll("g.node")
                .data(nodes, function (d) {
                    return d.id || (d.id = ++i);
                });

            var nodeEnter = node.enter().append("g")
                .attr("class", "node")
                .attr("transform", function (d) {
                    return "translate(" + source.y0 + "," + source.x0 + ")";
                })
                .style("opacity", 1e-6);

            // Enter any new nodes at the parent's previous position.
            nodeEnter.append("rect")
                .attr("y", -barHeight / 2)
                .attr("height", barHeight)
                .attr("width", barWidth)
                .style("fill", color)
                .on("click", click);

            nodeEnter.append("text")
                .attr("dy", 3.5)
                .attr("dx", 5.5)
                .text(function (d) {
                    return d.name;
                });

            // Transition nodes to their new position.
            nodeEnter.transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + d.y + "," + d.x + ")";
                })
                .style("opacity", 1);

            node.transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + d.y + "," + d.x + ")";
                })
                .style("opacity", 1)
                .select("rect")
                .style("fill", color);

            // Transition exiting nodes to the parent's new position.
            node.exit().transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + source.y + "," + source.x + ")";
                })
                .style("opacity", 1e-6)
                .remove();

            // Update the links�
            var link = svg.selectAll("path.link")
                .data(tree.links(nodes), function (d) {
                    return d.target.id;
                });

            // Enter any new links at the parent's previous position.
            link.enter().insert("path", "g")
                .attr("class", "link")
                .attr("d", function (d) {
                    var o = {x: source.x0, y: source.y0};
                    return diagonal({source: o, target: o});
                })
                .transition()
                .duration(duration)
                .attr("d", diagonal);

            // Transition links to their new position.
            link.transition()
                .duration(duration)
                .attr("d", diagonal);

            // Transition exiting nodes to the parent's new position.
            link.exit().transition()
                .duration(duration)
                .attr("d", function (d) {
                    var o = {x: source.x, y: source.y};
                    return diagonal({source: o, target: o});
                })
                .remove();

            // Stash the old positions for transition.
            nodes.forEach(function (d) {
                d.x0 = d.x;
                d.y0 = d.y;
            });
        };
        var svg = d3.select(selector).append("svg")
            //.attr("width", width + margin.left + margin.right)
            .attr("width", "100%").attr("height", "100%")
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        nodes.x0 = 800;

        nodes.y0 = 0;

        self.update(root = nodes);



        // Toggle children on click.
        function click(d) {
            if (d.children) {
                d._children = d.children;
                d.children = null;
            } else {
                d.children = d._children;
                d._children = null;
            }
            self.update(d);
        }

        function color(d) {
            return d._children ? "#3182bd" : d.children ? "#c6dbef" : "#fd8d3c";
        }
    };

    this.redraw = function () {
        self.vis.attr("transform",
            "translate(" + d3.event.translate + ")"
            + " scale(" + d3.event.scale + ")");
    };

    // Toggle children on click.
    this.forceclick = function (d) {
        if (d3.event.defaultPrevented) return; // ignore drag
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
        //self.update(d);
    };
    /* Define the main worker or execution function */
    this.makeForceDiag = function (node_data, links) {
        /* var my_nodes = $.map(node_data, function(d){return {"name": d.name};});
         var my_links = $.map(links, function(d){return {"source": d.source, "target": d.target, "value":d.target, "origin": d};});*/


        /* Establish the dynamic force behavor of the nodes */
        var force = d3.layout.force()
            .nodes(node_data)
            .links(links)
            .size([this.w, this.h])
            .linkDistance([250])
            .charge(-500)
            .gravity(0.05)
            .start();
        /* Draw the edges/links between the nodes */
        var edges = this.vis.selectAll("line")
            .data(links)
            .enter()
            .append("line")
            .style("stroke", "#ccc")
            .style("stroke-width", 1)
            .style("marker-end", "url(#suit)");
        /* Draw the nodes themselves */

        var nodes = this.vis.selectAll("circle")
            .data(node_data)
            .enter()
            .append("circle")
            .attr("r", function(d){
                if (d.type()!=undefined && d.type().length>0 && d.type()[0].indexOf('Class')>0){
                    return 25;
                }
                return 10;
            })
            .attr("opacity", 1)
            .attr("class", "node")
            .style("fill", function (d, i) {
                return (d.isType? "skyblue" : self.color(i));
            });

        var txt = this.vis.append('text')
                .attr("font-size", "28")
                .attr("font-weight", "bold")
                .attr("z-index", "100")
                .attr({ transform: 'translate(5,20)', fill:'lightcoral'})
                .text("");

        function highlightNeighbors(d,i) {
            var nodeNeighbors = findNeighbors(d,i);
            d3.selectAll("g.node").each(function(p) {
                 var isNeighbor = nodeNeighbors.nodes.indexOf(p);
                  d3.select(this).select("circle")
                  .style("opacity", isNeighbor > -1 ? 1 : .1)
                  .style("stroke-width", isNeighbor > -1 ? 3 : 0.5)
                  .style("stroke", isNeighbor > -1 ? "blue" : "white");
        });

        d3.selectAll("line")
        .style("stroke-width", function (d) {return nodeNeighbors.links.indexOf(d) > -1 ? 3 : 1})
        .style("opacity", function (d) {return nodeNeighbors.links.indexOf(d) > -1 ? 1 : .1});
            //.style("stroke", function (d) {return nodeNeighbors.links.indexOf(d) > -1 ? "black" : "#999"})
      }

        function findNeighbors(d,i) {
        var neighborArray = [d];
        var linkArray = [];
        d3.selectAll("line").filter(function(p) {
            return p.source == d || p.target == d}).each(function(p) {
                  neighborArray.indexOf(p.source) == -1 ? neighborArray.push(p.source) : null;
                  neighborArray.indexOf(p.target) == -1 ? neighborArray.push(p.target) : null;
                  linkArray.push(p);
        });

        return {nodes: neighborArray, links: linkArray};
      }

        function nodeOut() {
            var mousePos = d3.mouse(this);
            txt.attr({transform: 'translate(' + mousePos + ')'});
            d3.select(this).style("opacity", 0.8);
            d3.selectAll(".hoverLabel").remove();
            d3.selectAll("circle").style("opacity", 1).style("stroke", "white").style("stroke-width", "1px");
            d3.selectAll("text").style("opacity", 1);
            d3.selectAll("line").style("opacity", .25);
          }

        function nodeOver(d,i,e) {
            var el = this;
            if (!d3.event.fromElement) {
              el = e;
            }
            //if (nodeFocus) {
            //  return;
            //}
            var mousePos = d3.mouse(this);
            //if(d.type().indexOf("#")>0){
            //    txt.text(d.type().split("#")[1]);
            //}
            //else{
            //    txt.text(d.type());
            //}
            txt.attr({transform: 'translate(' + mousePos + ')'});
                //d3.select(this).style("opacity", 1.0)
            //Only do the element stuff if this came from mouseover

            /*el.parentNode.appendChild(el);
            d3.select(el).append("text").attr("class", "hoverLabel").attr("stroke", "white").attr("stroke-width", "5px")
            .style("opacity", 1)
            .style("pointer-events", "none")
            .text(d.label);

            d3.select(el).append("text").attr("class", "hoverLabel")
            .style("pointer-events", "none")
            .text(d.label);*/
            highlightNeighbors(d,i);
        }

        // register node events
        nodes.on("mouseover", focus_nodes)
             //.on("mouseout", nodeOut)
            .on('click', focus_nodes).call(force.drag);


        function focus_nodes(node,i,e){
                        //Toggle stores whether the highlighting is on
                        var toggle = 0;

                        var neighbors = findNeighbors(node,i);
                        connectedNodes(node, neighbors, this);

                        function connectedNodes(node, nodeNeighbors, t) {
                            var self = this;
                            this.nodeNeighbors = nodeNeighbors;
                            if (toggle == 0) {
                                //Reduce the opacity of all but the neighbouring nodes

                                d3.selectAll("circle").style("opacity", function (o) {
                                    return self.nodeNeighbors.nodes.indexOf(o) > -1 ? 1 : 0.1;
                                });
                                 d3.selectAll("text").style("opacity", function (o) {
                                    return self.nodeNeighbors.nodes.indexOf(o) > -1 ? 1 : 0.1;
                                });
                               d3.selectAll("line").style("opacity", function (o) {
                                    return self.nodeNeighbors.links.indexOf(o) > -1 ? 1 : 0.1;
                                });

                                toggle = 1;
                            } else {
                                //Put them back to opacity=1
                                d3.selectAll("circle").style("opacity", 1);
                                d3.selectAll("text").style("opacity", 1);
                                d3.selectAll("line").style("opacity", 1);
                                toggle = 0;
                            }

                        }

        }


        /* Draw the node labels first */
        var texts = this.vis.selectAll("text")
            .data(node_data)
            .enter()
            .append("text")
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "10px")
            .on("mouseover", function (d) {
                if (d.type()!= undefined){
                    d3.select(this).style("title", d.type().indexOf("#")>0?d.type().split("#")[1]:d.type()).attr("class", "hoverLabel")
                }
                else{
                     d3.select(this).attr("class", "hoverLabel")
                }
            })
            .text(function (d) {
                return d.shortName() || d.name();
            });

        nodes.append("svg:title")
            .text(function(d) {
                    var attrs = "";
                    $.each(d.attributes(), function(k,v){
                        attrs += JSON.stringify(v) + "\r\n";
                    });
                    return (d.shortName() || d.name()) + "\r\n" + attrs; });

        /* Run the Force effect */
        force.on("tick", function () {
            edges.attr("x1", function (d) {
                return d.source.x+1;
            }).attr("y1", function (d) {
                return d.source.y+1;
            }).attr("x2", function (d) {
                return d.target.x+1;
            }).attr("y2", function (d) {
                return d.target.y+1;
            });

            nodes.attr("cx", function (d) {
                return d.x;
            }).attr("cy", function (d) {
                return d.y;
            });
            //nodes.each(collide(0.5, nodes)); //Added
            texts.attr("transform", function (d) {
                return "translate(" + d.x + "," + d.y + ")";
            });
        }); // End tick func
    }; // End makeDiag worker func

function collide(alpha, nodes) {
    var padding = 1, // separation between circles
        radius=8;
  var quadtree = d3.geom.quadtree(nodes);
  return function(d) {
    var rb = 2*radius + padding,
        nx1 = d.x - rb,
        nx2 = d.x + rb,
        ny1 = d.y - rb,
        ny2 = d.y + rb;
    quadtree.visit(function(quad, x1, y1, x2, y2) {
      if (quad.point && (quad.point !== d)) {
        var x = d.x - quad.point.x,
            y = d.y - quad.point.y,
            l = Math.sqrt(x * x + y * y);
          if (l < rb) {
          l = (l - rb) / l * alpha;
          d.x -= x *= l;
          d.y -= y *= l;
          quad.point.x += x;
          quad.point.y += y;
        }
      }
      return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
    });
  };
}

};

